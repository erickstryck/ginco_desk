import Store from '../Util/Store';
import Constants from '../Util/Constants';


import errorImage from '../../public/images/pictures/error-img.png';

var instance;
/**
 * Node Reflexivo Voltado a Iteração com o Sistema
 */
export default class CentralCommand {

  /**
   * Função responsável por recuperar a instância singleton de CentralCommand.
   */
  static getInstance() {
    if (instance) {
      return instance;
    } else {
      instance = new CentralCommand();
      return instance;
    }
  }
  /**
   * Executa a ação requisitada importando automáticamente os itens no controller
   * 
   * @param {string} target 
   * @param {string} method 
   * @param {array} param 
   * @param {array} construct
   */
  executeAction(target, method, param = [], construct = []) {
    construct.push(Store.getInstance());
    this.process(target, method, param, construct);
  }

  /**
   * Método responsável por executar a reflexão encima dos parametro informados
   * 
   * @param {string} target 
   * @param {string} method 
   * @param {array} params 
   * @param {array} construct
   */
  process(target, method, params, construct = []) {
    if(!params){
      params = [];
    }

    require.ensure([], function () {
      try {
        let invoke = require('' + target); // a expressão ''+ é usada para fazer um casting explicito para string no js
        let classe = Reflect.construct(invoke.default, construct);
        if (Reflect.has(classe, method)) {
          Reflect.apply(classe[method], classe, params);
        } else {
          error = { message: 'O método ' + method + ' não foi encontrado!' };
          throw error;
        }
      } catch (e) {
        CentralCommand.processError(e.toString() + " in Class " + target + " and Method " + method);
        console.error(e);
      }
    });
  }

  /**
   * Método resposável por identificar e notificar o usuário sobre uma exceção
   * 
   * @param {object} errorMessage 
   */
  static processError(errorMessage) {
    let store = Store.getInstance();

    if (errorMessage) {
      store.modalMessage = errorMessage;
    } else {
      store.modalMessage = Constants.FATAL_ERROR;
    }

    store.modalImage = errorImage;
    store.modalCancelable = true;
    store.modal = true;
  }
}