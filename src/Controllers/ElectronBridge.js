import CentralCommand from './CentralCommand';
import { remote } from 'electron';

import loadingImage from '../../public/images/pictures/loading-img.png';
import errorImage from '../../public/images/pictures/error-img.png';


/**
 * Classe responsável pela comunicação com os componentes do electron.
 * 
 * Todas as chamadas de funções contidas aqui só serão válidas e executadas dentro do container do electron, no momento de desenvolvimento
 * web tais chamadas não estarão disponíveis e chamá-las fora da aplicação electron poderá gerar erros no console.
 */
export default class ElectronBridge {

  constructor(labels, store) {
    this.store = store;
    this.labels = labels;
  }

  /**
   * Fecha a aplicação pelo electron
   */
  closeAplication() {
    remote.getCurrentWindow().close();
  }

  /**
   * Minimiza a aplicação pelo electron
   */
  minimizeAplication() {
    remote.getCurrentWindow().minimize();
  }


  /**
   * Função responsável por restaurar os dados do último save.
   */
  restoreSaveData() {
    this.store.modalMessage = this.labels.LABEL_LOAD_SAVE_DATA;
    this.store.modalCancelable = false;
    this.store.modalImage = loadingImage;
    this.store.modal = true;

    remote.getGlobal('db').findOne({ _id: 'gincodata' }, function(error, docs){
      if (error) {
        CentralCommand.processError(this.labels.LABEL_FAIL_SAVE_LOAD);
        console.error(error);
      } else if(docs) {
        //conversão necessária para eleminiar os watches do bd e inserir os dados limpos na store.
        docs = JSON.parse(JSON.stringify(docs));
        this.store.modalCancelable = true;
        this.store.modal = false;
        
        Object.keys(docs).forEach((key) => {
          if(key != "_id"){
            this.store[key] = docs[key];
          }
        });
      }else{
        this.store.modalMessage = this.labels.LABEL_EMPTY_SAVE_DATA;
        this.store.modalCancelable = true;
        this.store.modalImage = errorImage;
        this.store.modal = true;
      }

    }.bind(this));

    
  }

  /**
   * Salva os dados da store em um cookie para que possa ser resgatado posteriormente.
   */
  saveDataInCookie() {
    this.store.modalFooter = null;
    this.store.modalMessage = this.labels.LABEL_SAVING;
    this.store.modalCancelable = false;
    this.store.modalImage = loadingImage;
    this.store.modal = true;

    remote.getGlobal('db').remove({}, { multi: true }, function(err){
      remote.getGlobal('db').insert(this.getStoreSaveData(), function(err, newDoc){
        if (err) {
          CentralCommand.processError(this.labels.LABEL_FAIL_SAVE);
          console.error(error);
        } else {
          this.store.modalCancelable = true;
          this.store.modal = false;
        }
      }.bind(this));
    }.bind(this));
  }

  /**
   * Recupera os dados da store para que possa ser persistiodo no cookie do electrom.
   */
  getStoreSaveData() {
    return {
      _id: "gincodata",
      lang: this.store.langData,
      muted: this.store.mutedData,
      balancingTimes: this.store.balancingTimesData,
      lostTeams: this.store.lostTeamsData,
      teamsSize: this.store.teamsSizeData,
      occurredBattles: this.store.occurredBattlesData,
      battleLevel: this.store.battleLevelData,
      nextBattle: this.store.nextBattleData,
      customWidth: this.store.customWidthData,
      isValidTeamNumber: this.store.isValidTeamNumberData,
      gincana: this.store.gincanaData,
      teams: this.store.teamsData,
      route: this.store.routeData
    };
  }

}