
import Constants from '../Util/Constants';
import Store from '../Util/Store'

/**
 * Classe responsável pela importação dos dados web
 */
export default class GincanaController {

  constructor(labels) {
    this.store = Store.getInstance();
    this.labels = labels;
  }

  /**
   * Função responsável por verificar se o número de equipes está balanceado.
   * 
   * @param {number} teamNumber 
   */
  validateTeamNumber(teamNumber) {
    let divVal = teamNumber / 2;
    if (divVal > 1 && divVal % 2 == 0) {
      return this.validateTeamNumber(divVal);
    } else if (divVal == 1) {
      return true;
    } else return false;
  }

  /**
   * Recupera o número de vezes que deverá ocorrer um balanceamento.
   */
  setNumberOfBalancingTimesStore() {
    let balanceTimes = 0;
    let teamSize = this.store.teamsSizeData;
    while (!this.validateTeamNumber(teamSize)) {
      --teamSize;
      ++balanceTimes;
    }

    this.store.balancingTimes = balanceTimes;
  }

  /**
   * Atualiza a store com a informação da próxima batalha
   */
  getNextBattle() {
    if (this.validateTeamNumber(this.store.occurredBattles) && this.store.occurredBattles == this.store.teams[this.store.battleLevel].length / 2) {
      //reseta o numero de batalhas ocorridas naquele nível
      this.store.occurredBattles = 0;
      //atualiza o indice do array que dita qual o nivel do ranquing está ocorrendo as batalhas
      ++this.store.battleLevel;
    }

    //atualiza o indice que dita quais os proximos a se enfrentarem no nível atual.
    this.store.nextBattle.team.index = this.store.occurredBattles > 0 ? this.store.occurredBattles + this.store.occurredBattles : 0;
    this.store.nextBattle.teamVs.index = this.store.nextBattle.team.index + 1;
  }

  /**
   * Inicia o jogo após a página de introdução da gincana.
   * 
   * @param {function} toPage 
   */
  startGame(toPage) {
    if (this.validateTeamNumber(this.store.teamsSize)) {
      toPage(Constants.RANKING_KEY);
    } else {
      this.setNumberOfBalancingTimesStore();
      toPage(Constants.RANKING_BALANCE_KEY);
    }
  }

  /**
   * Recupera que ainda não foi sorteado do array para efetuar o balanceamento.
   */
  getIndexTeamToBalance() {
    let index = Math.floor((Math.random() * this.store.teamsSize));
    if (this.store.teams[0][index].isBalanced) {
      index = this.getIndexTeamToBalance();
    } else {
      this.store.teams[0][index].isBalanced = true;
    }

    return index;
  }

  /**
   * Atualiza o array de times que foram balanceados
   */
  updateTeamsBalaced() {
    let indexTeam1 = this.store.nextBattle.team.index;
    let indexTeam2 = this.store.nextBattle.teamVs.index;

    if (indexTeam1 != indexTeam2) {
      let teams = this.store.teamsData[0];
      let indexWinner = 0;

      if (teams[indexTeam1].winner) {
        //retirar a flag do ganhador para nao influenciar na visualização do ranking posteriormente.
        delete teams[indexTeam1].winner;
        indexWinner = indexTeam2 < indexTeam1 ? --indexTeam1 : indexTeam1;

        //guarda o perdedor do balanceamento na store para aparecer nos creditos.
        this.store.lostTeams.push(this.store.teamsData[0][indexTeam2]);

        teams.splice(indexTeam2, 1);
      } else {
        //retirar a flag do ganhador para nao influenciar na visualização do ranking posteriormente.
        delete teams[indexTeam2].winner;
        indexWinner = indexTeam1 < indexTeam2 ? --indexTeam2 : indexTeam2;

        //guarda o perdedor do balanceamento na store para aparecer nos creditos.
        this.store.lostTeams.push(this.store.teamsData[0][indexTeam1]);

        teams.splice(indexTeam1, 1);
      }

      this.store.nextBattle.team.index = indexWinner;
      this.store.nextBattle.teamVs.index = indexWinner;

      this.store.teams[0] = teams;

      --this.store.teamsSize;
      --this.store.balancingTimes;

      if (!this.store.balancingTimes) {
        --this.store.occurredBattles;
      }
    }

  }

  /**
   * Atualiza a store com a informação da próxima batalha do balanceamento
   */
  getNextBattleBalance() {
    if (this.validateTeamNumber(this.store.teamsSize)) {
      this.store.route = Constants.RANKING_KEY;
    }

    let indexTeam1 = this.getIndexTeamToBalance();
    let indexTeam2 = this.getIndexTeamToBalance();

    this.store.nextBattle.team.index = indexTeam1;
    this.store.nextBattle.teamVs.index = indexTeam2;
  }

}