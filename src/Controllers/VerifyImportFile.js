import CentralCommand from './CentralCommand';
import Constants from '../Util/Constants';

/**
 * Classe responsável pela importação dos dados web
 */
export default class VerifyImportFile {

  constructor(labels, store) {
    this.store = store;
    this.labels = labels;
    this.isValid = true;
  }

  /**
   * Valida as equipes no arquivo importado.
   * 
   * @param {Object} gincana 
   */
  validateTeams(gincana) {
    gincana.equipes.some((team) => {
      if (
        team.nome &&
        team.message &&
        team.image &&
        team.alunos &&
        team.perguntas &&
        this.isValid
      ) {
        this.validateStudent(team.alunos);
        this.validateQuestions(team.perguntas);
      } else {
        this.isValid = false;
        return false;
      }
    }, this);

    return this.isValid;
  }

  /**
   * Verifica e recupera a informação importada.
   * 
   * @param {*} jsonData 
   */
  getValidJsonData(jsonData) {
    let jsonValidObject = '';
    try {
      jsonValidObject = JSON.parse(jsonData);
    }
    catch (e) {
      jsonValidObject = {};
    }

    return jsonValidObject;
  }

  /**
   * Valida os alunos no arquivo importado.
   * 
   * @param {Object} students
   */
  validateStudent(students) {
    students.some((student) => {
      if (!(student.nome && this.isValid)) {
        this.isValid = false;
        return false;
      }
    }, this);
  }

  /**
   * Valida os perguntas no arquivo importado.
   * 
   * @param {Object} questions
   */
  validateQuestions(questions) {
    questions.some((question) => {
      if (question.enunciado && question.alternativas && this.isValid) {
        this.validateAlternatives(question.alternativas);
      } else {
        this.isValid = false;
        return false;
      }
    }, this);
  }

  /**
   * Valida as alternativas no arquivo importado.
   * 
   * @param {Object} alternatives
   */
  validateAlternatives(alternatives) {
    alternatives.some((alternative) => {
      if (!(alternative.descricao && alternative.isCorreta !== undefined && this.isValid)) {
        this.isValid = false;
        return false;
      }
    }, this);
  }

  /**
   * Função responsável por verificar a integridade do json de importação
   * 
   * @param {Object} jsonData 
   * @param {Function} toPage 
   */
  verifyImport(jsonData, toPage) {
    this.store.modalVerifyImport = true;

    jsonData = this.getValidJsonData(jsonData);

    if (jsonData.gincana && jsonData.gincana.nome && jsonData.gincana.professor && jsonData.gincana.tema && jsonData.gincana.time && this.validateTeams(jsonData.gincana)) {
      let teamsProcessed = [];
      jsonData.gincana.equipes.forEach(team => {
        team.victoriesNumber = 0;
        team.tempVictories = 0;
        teamsProcessed.push(team);
      });

      delete jsonData.gincana.equipes;
      this.store.gincana = jsonData.gincana;
      this.store.teams.push(teamsProcessed);
      this.store.teamsSize = teamsProcessed.length;

      toPage(Constants.INTRO_KEY);
    } else {
      this.store.modalVerifyImport = false;
      CentralCommand.processError(this.labels.JSON_IMPORT_ERROR);
    }
  }
}