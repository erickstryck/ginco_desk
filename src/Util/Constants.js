import React, { Component } from 'react';

/**
 * Módulo responsável por prover as constants do sistema
 */
class Constants extends Component {

  constructor() {
    super();
    this.constants = {

      //DeathStar keys
      WELCOME_KEY: 'WELCOME_KEY',
      DATA_LOAD_KEY: 'DATA_LOAD_KEY',
      RANKING_KEY: 'RANKING_KEY',
      BATTLE_KEY: 'BATTLE_KEY',
      FINAL_KEY: 'FINAL_KEY',
      INTRO_KEY: 'INTRO_KEY',
      COMMON_PAGE_CONTEXT_KEY: 'COMMON_PAGE_CONTEXT_KEY',
      TEAM_KEY: 'TEAM_ELEMENTS',
      TEAM_MAP_ITENS_KEY: 'TEAM_MAP_ITENS',
      BATTLE_INFO_KEY: 'BATTLE_INFO_KEY',
      BATTLE_QUEST_KEY: 'BATTLE_QUEST_KEY',
      RESULT_BATTLE_KEY: 'RESULT_BATTLE_KEY',
      CUBE_DECISION_KEY: 'CUBE_DECISION_KEY',
      RANKING_BALANCE_KEY: 'RANKING_BALANCE_KEY',
      TEAM_ELEMENTS_SIZE: 530,
      CONTEXT_RANKING_KEY: "CONTEXT_RANKING_KEY",

      //Languages
      PT_BR: {
        TITLE_APP_NAME: 'Ginco',
        TITLE_WELCOME: 'Bem vindo(a) ao Ginco',

        LABEL_START: 'Iniciar',
        LABEL_LOAD_SAVE: 'Carregar Último Salvo',
        LABEL_LOAD_SAVE_DATA: 'Carregando...',
        LABEL_EMPTY_SAVE_DATA: 'Sem informações para carregar!',
        LABEL_IMPORT_DATA: 'Importar Nova Gincana',
        LABEL_TEACHER_RESPON: 'Professor Responsável',
        LABEL_SOUND: 'Som',
        LABEL_ON: 'Ligado',
        LABEL_OFF: 'Desligado',
        LABEL_DATA_VERIFY: 'Verificando arquivo',
        LABEL_DATA_ERROR: 'Houve um problema no arquivo',
        LABEL_PROCESS_DATA_ERROR: 'Houve um erro ao processar os dados importados!',
        LABEL_BATTLE_INFO: 'Batalha Atual',
        LABEL_BALANCE_BATTLE_INFO: 'Batalha de Balanceamento',
        LABEL_BACK: 'Voltar',
        LABEL_BATTLE_ROUND: 'Pergunta ',
        LABEL_BATTLE_WHO: 'Quem responde',
        LABEL_ACTUAL_BATTLE: 'Vitórias nesta batalha',
        LABEL_WITHOUT_VICTORIES: 'Não há vitórias nesta batalha',
        LABEL_QUESTION: 'Pergunta',
        LABEL_ALTERNATIVES: 'Alternativas',
        LABEL_OK: 'Ok',
 
        LABEL_TEAM_NAME: 'Nome da equipe: ',
        LABEL_TEAM_MESSAGE: 'Grito de Guerra: ',
        LABEL_VICTORIES: 'Vitórias: ',
        LABEL_OPEN_MAP: 'Abrir Mapa',
        LABEL_NEXT: 'Próxima',
        LABEL_BATTLE: 'Batalha',
        LABEL_SAVE: 'Salvar',
        LABEL_SAVING: 'Salvando...',
        LABEL_QUESTION_SAVING: 'Salvar sobrescreverá as últimas informações salvas!',
        LABEL_EXIT: 'Sair',
        LABEL_MINIMIZE: 'Minimizar',

        LABEL_EVENT: 'Gincana',
        LABEL_EVENT_THEME: 'Tema',
        LABEL_TEAM_MAP: 'Mapa de Equipes',
        LABEL_TEAM_MAP_RANKING: 'Colocação das Equipes',

        LABEL_TIME_IS_OVER: 'Oops, acabou o tempo.',
        LABEL_WRONG_ANSWER: 'Vocês erraram!',
        LABEL_RIGHT_ANSWER: 'Vocês acertaram!',
        LABEL_CHECK_ANSWER: 'E a resposta correta é',
        LABEL_NEXT_ROUND: 'Próxima pergunta',
        LABEL_PTS: 'PTs',
        LABEL_GOOD_LUCK: 'Desempate na sorte',
        LABEL_DICE_ROLL: 'Jogar Dado',
        LABEL_BATTLE_RESULTS: 'O vencedor é',
        LABEL_LAST_QUESTION: 'Pergunta Final',
        LABEL_LAST_BATTLE: 'Batalha Final',
        LABEL_RANKING : 'Ranking',
        LABEL_STUDENTS : 'Alunos',
        LABEL_HOME : 'Início',

        LABEL_BALANCE_INFO: "O número de equipes está desbalanceado",
        LABEL_BALANCE: "Balanceamento",
        LABEL_BALANCE_TIMES_INFO: ["Deverá ocorrer ", " batalha(s) para balancear o ranking"],

        LABEL_FAIL_SAVE: 'Houve um erro ao salvar!',
        LABEL_FAIL_SAVE_LOAD: 'Houve um erro ao recuperar as informações!',

        //Errors Message
        JSON_IMPORT_ERROR: "O arquivo importado é inválido"
      }
    };
  }

}

export default new Constants().constants;