import Dispatcher from './Dispatcher';
import Store from './Store';
import 'whatwg-fetch';

export default class Actions{

  execute(method, endpoint, action, model, obj){
    function status(response) {
      if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response.text());
      } else {
        return Promise.reject(response.text());
      }
    }

    let send = {
      method: method,
      headers: {
        'Accept': 'application/json',
        'Content-Type':method==='file'?undefined:'application/json',
        'X-Access-Token': Store.getToken()
      }
    };

    if(method === 'post' || method === 'put'){
      send.body = JSON.stringify(obj);
    }

    fetch(endpoint.url + '?uc=' + endpoint.controller + '&act=' + action, send)
    .then(status)
    .then((data) => {
      if(model){
        Dispatcher.dispatch({
          type: method,
          model: model,
          result: JSON.parse(data)
        });
      }
    }).catch((err) => {
      if(typeof err === 'object' && err.then instanceof Function){
        err.then((result) => {
          Dispatcher.dispatch({
            model: 'errors',
            result: result
          });
        });
      }else{
        Dispatcher.dispatch({
          model: 'errors',
          result: err
        });
      }
    });
  }
}
