import { observable, computed, toJS } from 'mobx';
import autobind from 'autobind-decorator';
import createHistory from 'history/createBrowserHistory';
import Constants from './Constants';

const history = createHistory({ forceRefresh: true });

var store = '';

/**
 * Classe resposável por prover o storage de informações que serão consumida pela aplicação
 */
@autobind
export default class Store {

  /**
   * Variáveis observáveis
   */
  @observable lang = 'PT_BR';
  @observable route = Constants.WELCOME_KEY;
  @observable muted = false;
  @observable volume = 0.4;
  @observable modalVerifyImport = false;
  //modal de informaçoes gerais
  @observable modal = false;
  @observable modalMessage = false;
  @observable modalImage = '';
  @observable modalCancelable = true;
  modalFooter = null;//Não pode ser observavel por conter elementos jsx.
  @observable gincana = {};//tempo minimo das pergunstas e de 10s
  //Matriz onde cada índice deste array será um nível do ranking com um array de times.
  @observable teams = [];//numero de perguntas dos times = (numero de níveis * 4) + 4
  @observable teamsSize = 0;
  @observable occurredBattles = 0;//batalhas ocorridas no nível atual
  @observable battleLevel = 0;//Nivel atual do ranking que está acontecendo as batalhas
  @observable balancingTimes = 0;
  @observable lostTeams = [];
  @observable nextBattle = {
    team: {
      index: 0,
      tempVictories: 0,
      questionIndex : 0,
    },
    teamVs: {
      index: 0,
      tempVictories: 0,
      questionIndex : 0,
    },
    step: 1//numero de rodadas de cada batalha max=6 + dado
  };
  @observable customWidth = "";
  @observable isValidTeamNumber = false;

  //funções responsáveis por retornar as regras em um estado não observável
  @computed get langData() {
    return toJS(this.lang);
  }

  @computed get routeData() {
    return toJS(this.route);
  }

  @computed get mutedData() {
    return toJS(this.muted);
  }

  @computed get balancingTimesData() {
    return toJS(this.balancingTimes);
  }
  
  @computed get lostTeamsData() {
    return toJS(this.lostTeams);
  }

  @computed get teamsSizeData() {
    return toJS(this.teamsSize);
  }

  @computed get occurredBattlesData() {
    return toJS(this.occurredBattles);
  }

  @computed get battleLevelData() {
    return toJS(this.battleLevel);
  }

  @computed get nextBattleData() {
    return toJS(this.nextBattle);
  }

  @computed get customWidthData() {
    return toJS(this.customWidth);
  }

  @computed get isValidTeamNumberData() {
    return toJS(this.isValidTeamNumber);
  }

  @computed get gincanaData() {
    return toJS(this.gincana);
  }

  @computed get teamsData() {
    return toJS(this.teams);
  }

  /**
   * Reseta as variaveis sensíveis da store para o reinicio de uma gincana
   */
  resetStore(){
    this.lang = 'PT_BR';
    this.muted = false;
    this.volume = 0.4;
    this.gincana = {};
    this.teams = [];
    this.teamsSize = 0;
    this.occurredBattles = 0;
    this.battleLevel = 0;
    this.balancingTimes = 0;
    this.lostTeams = [];
    this.customWidth = "";
    this.isValidTeamNumber = false;
    this.modalVerifyImport = false;
    this.resetNextBattle();
  }

  /**
   * Reseta as batalhas temporarias para que novas ocorram.
   */
  resetNextBattle() {
    this.nextBattle = {
      team: {
        index: 0,
        tempVictories: 0,
        questionIndex : 0
      },
      teamVs: {
        index: 0,
        tempVictories: 0,
        questionIndex : 0
      }
    };
  }

  /**
   * construtor
   */
  constructor() {
    this.history = history;
  }

  /**
  * Método responsável por prover o singleton para o store
  */
  static getInstance() {
    if (store) return store;
    else {
      store = new Store();
      return store;
    }
  }

  /**
   * Cria um ID unico para ser utilizado no sistema. 
   */
  getId() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '_' + s4() + '_' + s4() + '_' +
      s4() + '_' + s4() + s4() + s4();
  }

}