import React, { Component } from 'react';
import { Router, Switch, Route } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import CentralCommand from './Controllers/CentralCommand';
import Store from './Util/Store';
import CommonPage from 'Pages/Common/CommonPage';
import Constants from './Util/Constants';

/**
 * Classe resposável por definir todas as rotas do sistema
 */
export default class App extends Component {

  /**
	 * Construtor da classe
	 */
  constructor() {
    super();
    this.state = {
      centralCommand: CentralCommand.getInstance(),
      store: Store.getInstance()
    }
  }

  /**
   * É por meio deste render que renderizamos as rotas no index.js
   */
  render() {
    //No momento temos apenas linguagem em PT-BR
    return (
      <Router history={createBrowserHistory()} >
        <Switch>
          <Route path='/' render={() => <CommonPage constants={Constants} centralCommand={this.state.centralCommand} labels={Constants[this.state.store.lang]} store={this.state.store} />} />
        </Switch>
      </Router>
    );
  }
};