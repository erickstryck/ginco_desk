import React, { Component } from 'react';
import { Row, Col, Layout, Button, Upload, Modal } from 'antd';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import '../../../public/css/dataLoad/dataLoad.css';

import disketImage from '../../../public/images/pictures/save-img.png';
import importImage from '../../../public/images/pictures/import-img.png';
import speakImage from '../../../public/images/pictures/speak-img.png';
import muteSpeakImage from '../../../public/images/pictures/mute-speak-img.png';
import loadingImage from '../../../public/images/pictures/loading-img.png';

const { Header, Content } = Layout;

/**
 * Componente Responsável por mostrar as opções iniciais
 * 
 */
@observer
@autobind
export default class DataLoad extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      upload: {
        name: 'dataLoad',
        accept: ".json,application/json",
        customRequest: () => { return false; },
        showUploadList: false,
        multiple: false,
        onChange: this.processDataUploated
      }
    }
  }

  /**
   * Faz o meio de campo entre o componente de upload e a row em que está a área clicavel.
   */
  handleUploadData() {
    document.getElementById('hiddenUpload').click();
  }

  /**
   * Abre a tela de classificação das equipes
   */
  toIntroPage() {
    document.getElementById('dataLoad').className = 'goOutPage borderFrame';
    setTimeout(() => { this.props.store.route = this.props.constants.INTRO_KEY }, 1000);
  }

  /**
   * Processa os arquivo enviado para dar carga nas gincanas.
   * 
   * @param {Object} file 
   */
  processDataUploated(info) {
    let reader = new FileReader();
    reader.onload = function (event) {
      this.props.centralCommand.executeAction('./VerifyImportFile', 'verifyImport', [event.target.result, this.toIntroPage], [this.props.labels]);
    }.bind(this);
    reader.readAsText(info.file.originFileObj);
  }

  /**
   * Recupera o save da ultima partida salva
   */
  restoreDataHandleFunction() {
    this.props.centralCommand.executeAction('./ElectronBridge', 'restoreSaveData', null, [this.props.labels]);
  }

  /**
    * Renderiza o componente
    */
  render() {
    //variveis observadas da store

    let elementJSX = <div />;
    if (this.props.renderElement) {

      elementJSX =
        <div className="goInPage borderFrame" id="dataLoad">
          <Modal
            visible={this.props.store.modalVerifyImport}
            onCancel={() => { this.props.store.modalVerifyImport = false; }}
            className='modalBGColorInfo borderModal'
            maskClosable={false}
            closable={false}
            mask={false}
            footer={null}
          >
            <div className="modalVerify">
              <Row type='flex' justify="center" align="middle">
                <Col>
                  <p className="fontVcrOsdMono verifyFontSize">{this.props.labels.LABEL_DATA_VERIFY}</p>
                </Col>
              </Row>
              <Row type='flex' justify="center" align="middle">
                <Col>
                  <img src={loadingImage} />
                </Col>
              </Row>
            </div>
          </Modal>
          <Upload {...this.state.upload}>
            <Button style={{ display: "none" }} id="hiddenUpload"></Button>
          </Upload>
          <Row type='flex' justify="center" align="middle" className="optionsRow" onClick={this.handleUploadData}>
            <Col>
              <img src={importImage} />
            </Col>
            <Col offset={1} span={8}>
              <span className="fontVcrOsdMono">{this.props.labels.LABEL_IMPORT_DATA}</span>
            </Col>
          </Row>
          <Row type='flex' justify="center" align="middle" className="optionsRow" onClick={this.restoreDataHandleFunction}>
            <Col>
              <img src={disketImage} />
            </Col>
            <Col offset={1} span={8} >
              <span className="fontVcrOsdMono">{this.props.labels.LABEL_LOAD_SAVE}</span>
            </Col>
          </Row>
          <Row type='flex' justify="center" align="middle" className="optionsRow" onClick={this.props.commonFunctions.muteHandleFunction.bind(this, this.props)}>
            <Col>
              <img src={this.props.store.muted ? muteSpeakImage : speakImage} />
            </Col>
            <Col offset={1} span={8}>
              <span className="fontVcrOsdMono correctSize">
                <p>{this.props.labels.LABEL_SOUND} {this.props.store.muted ? this.props.labels.LABEL_OFF : this.props.labels.LABEL_ON}</p>
              </span>
            </Col>
          </Row>
        </div>;
    }

    return elementJSX;
  }
}
