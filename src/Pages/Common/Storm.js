var instance = '';

export default class Storm {

	initResources() {
		this.canvas1 = document.getElementById('canvas1');
		this.canvas2 = document.getElementById('canvas2');
		this.canvas3 = document.getElementById('canvas3');
		this.ctx1 = this.canvas1.getContext('2d');
		this.ctx2 = this.canvas2.getContext('2d');
		this.ctx3 = this.canvas3.getContext('2d');

		this.rainthroughnum = 500;
		this.speedRainTrough = 25;
		this.RainTrough = [];

		this.rainnum = 500;
		this.rain = [];

		this.lightning = [];
		this.lightTimeCurrent = 0;
		this.lightTimeTotal = 0;

		this.w = this.canvas1.width = this.canvas2.width = this.canvas3.width = window.innerWidth;
		this.h = this.canvas1.height = this.canvas2.height = this.canvas3.height = window.innerHeight;
		window.addEventListener('resize', function () {
			this.w = this.canvas1.width = this.canvas2.width = this.canvas3.width = window.innerWidth;
			this.h = this.canvas1.height = this.canvas2.height = this.canvas3.height = window.innerHeight;
		});

		this.animloopreference = '';
	}

	static getInstance(){
		if(!instance){
			instance = new Storm();
			instance.initResources();
		}

		return instance;
	}

	random(min, max) {
		return Math.random() * (max - min + 1) + min;
	}

	clearCanvas1() {
		this.ctx1.clearRect(0, 0, this.w, this.h);
	}

	clearCanvas2() {
		this.ctx2.clearRect(0, 0, this.canvas2.width, this.canvas2.height);
	}

	clearCanvas3() {
		this.ctx3.globalCompositeOperation = 'destination-out';
		this.ctx3.fillStyle = 'rgba(0,0,0,' + this.random(1, 30) / 100 + ')';
		this.ctx3.fillRect(0, 0, this.w, this.h);
		this.ctx3.globalCompositeOperation = 'source-over';
	};

	createRainTrough() {
		for (var i = 0; i < this.rainthroughnum; i++) {
			this.RainTrough[i] = {
				x: this.random(0, this.w),
				y: this.random(0, this.h),
				length: Math.floor(this.random(1, 830)),
				opacity: Math.random() * 0.2,
				xs: this.random(-2, 2),
				ys: this.random(10, 20)
			};
		}
	}

	createRain() {
		for (var i = 0; i < this.rainnum; i++) {
			this.rain[i] = {
				x: Math.random() * this.w,
				y: Math.random() * this.h,
				l: Math.random() * 1,
				xs: -4 + Math.random() * 4 + 2,
				ys: Math.random() * 10 + 10
			};
		}
	}

	createLightning() {
		var x = this.random(100, this.w - 100);
		var y = this.random(0, this.h / 4);

		var createCount = this.random(1, 3);
		for (var i = 0; i < createCount; i++) {
			var single = {
				x: x,
				y: y,
				xRange: this.random(5, 30),
				yRange: this.random(10, 25),
				path: [{
					x: x,
					y: y
				}],
				pathLimit: this.random(40, 55)
			};
			this.lightning.push(single);
		}
	};

	drawRainTrough(i) {
		this.ctx1.beginPath();
		var grd = this.ctx1.createLinearGradient(0, this.RainTrough[i].y, 0, this.RainTrough[i].y + this.RainTrough[i].length);
		grd.addColorStop(0, "rgba(255,255,255,0)");
		grd.addColorStop(1, "rgba(255,255,255," + this.RainTrough[i].opacity + ")");

		this.ctx1.fillStyle = grd;
		this.ctx1.fillRect(this.RainTrough[i].x, this.RainTrough[i].y, 1, this.RainTrough[i].length);
		this.ctx1.fill();
	}

	drawRain(i) {
		this.ctx2.beginPath();
		this.ctx2.moveTo(this.rain[i].x, this.rain[i].y);
		this.ctx2.lineTo(this.rain[i].x + this.rain[i].l * this.rain[i].xs, this.rain[i].y + this.rain[i].l * this.rain[i].ys);
		this.ctx2.strokeStyle = 'rgba(174,194,224,0.5)';
		this.ctx2.lineWidth = 1;
		this.ctx2.lineCap = 'round';
		this.ctx2.stroke();
	}

	drawLightning() {
		for (var i = 0; i < this.lightning.length; i++) {
			var light = this.lightning[i];

			light.path.push({
				x: light.path[light.path.length - 1].x + (this.random(0, light.xRange) - (light.xRange / 2)),
				y: light.path[light.path.length - 1].y + (this.random(0, light.yRange))
			});

			if (light.path.length > light.pathLimit) {
				this.lightning.splice(i, 1);
			}

			this.ctx3.strokeStyle = 'rgba(255, 255, 255, .1)';
			this.ctx3.lineWidth = 3;
			if (this.random(0, 15) === 0) {
				this.ctx3.lineWidth = 6;
			}
			if (this.random(0, 30) === 0) {
				this.ctx3.lineWidth = 8;
			}

			this.ctx3.beginPath();
			this.ctx3.moveTo(light.x, light.y);
			for (var pc = 0; pc < light.path.length; pc++) {
				this.ctx3.lineTo(light.path[pc].x, light.path[pc].y);
			}
			if (Math.floor(this.random(0, 30)) === 1) { //to fos apo piso
				this.ctx3.fillStyle = 'rgba(255, 255, 255, ' + this.random(1, 3) / 100 + ')';
				this.ctx3.fillRect(0, 0, this.w, this.h);
			}
			this.ctx3.lineJoin = 'miter';
			this.ctx3.stroke();
		}
	};

	animateRainTrough() {
		this.clearCanvas1();
		for (var i = 0; i < this.rainthroughnum; i++) {
			if (this.RainTrough[i].y >= this.h) {
				this.RainTrough[i].y = this.h - this.RainTrough[i].y - this.RainTrough[i].length * 5;
			} else {
				this.RainTrough[i].y += this.speedRainTrough;
			}
			this.drawRainTrough(i);
		}
	}

	animateRain() {
		this.clearCanvas2();
		for (var i = 0; i < this.rainnum; i++) {
			this.rain[i].x += this.rain[i].xs;
			this.rain[i].y += this.rain[i].ys;
			if (this.rain[i].x > this.w || this.rain[i].y > this.h) {
				this.rain[i].x = Math.random() * this.w;
				this.rain[i].y = -20;
			}
			this.drawRain(i);
		}
	}

	animateLightning() {
		this.clearCanvas3();
		this.lightTimeCurrent++;
		if (this.lightTimeCurrent >= this.lightTimeTotal) {
			this.createLightning();
			this.lightTimeCurrent = 0;
			this.lightTimeTotal = 200;  //rand(100, 200)
		}
		this.drawLightning();
	}

	init() {
		document.getElementsByClassName('stormThunder')[0].classList.add('thunder');
		document.getElementsByClassName('stormThunder')[0].id = 'storm';
		this.createRainTrough();
		this.createRain();
		window.addEventListener('resize', this.createRainTrough);
	}

	animLoop() {
		this.animateRainTrough();
		this.animateRain();
		this.animateLightning();
		this.animloopreference = requestAnimationFrame(this.animLoop.bind(this));
	}

	stopAnimLoop() {
		document.getElementsByClassName('stormThunder')[0].classList.remove('thunder');
		document.getElementsByClassName('stormThunder')[0].id = '';
		cancelAnimationFrame(this.animloopreference);
		this.ctx1.clearRect(0, 0, this.canvas1.width, this.canvas1.height);
		this.ctx2.clearRect(0, 0, this.canvas2.width, this.canvas2.height);
		this.ctx3.clearRect(0, 0, this.canvas3.width, this.canvas3.height);
	}
}