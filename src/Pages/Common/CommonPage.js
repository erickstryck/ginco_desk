import React, { Component } from "react";
import autobind from "autobind-decorator";
import { LocaleProvider, Modal, Row, Col, Tooltip, Button } from "antd";
import pt_BR from "antd/lib/locale-provider/pt_BR";
import "moment/locale/pt-br";
import { observer } from "mobx-react";
import "../../../public/css/util.css";
import "../../../public/css/animate.css";
import "../../../public/css/rain.css";

import DeathStar from "../../Lib/DeathStar";
import Storm from "./Storm.js";

import DataLoad from "../DataLoad/DataLoad";
import Welcome from "../Welcome/Welcome";
import Ranking from "../Ranking/Ranking";
import BattleQuestion from "../Battle/BattleQuestion";
import BattleInfo from "../Battle/BattleInfo";
import ResultBattle from "../Battle/ResultBattle";
import CubeDecision from "../Battle/CubeDecision";
import RankingBalance from "../Ranking/RankingBalance";
import FinalPage from "../Final/FinalPage";
import IntroPage from "../Welcome/Intro";

//Audio musicas
import MainMelody from "../../../public/audio/main-melody.wav";
import IntroMelody from "../../../public/audio/intro-melody.mp3";
import Melody from "../../../public/audio/melody.wav";
import FinalMelody from "../../../public/audio/final-melody.mp3";
import Melody2 from "../../../public/audio/melody-2.wav";
import Melody3 from "../../../public/audio/melody-3.wav";
import Melody4 from "../../../public/audio/melody-4.wav";
import FinalBattleMelody from "../../../public/audio/final-battle-melody.wav";
import RoundMelody from "../../../public/audio/round-melody.mp3";
import CubeDecisionMelody from "../../../public/audio/cube_decision_melody.mp3";
import ResultBattleMelody from "../../../public/audio/result-battle-melody.wav";

//Backgrounds Dinâmicos
import bg1 from "../../../public/images/backgrounds/1.png";
import bg2 from "../../../public/images/backgrounds/2.png";
import bg3 from "../../../public/images/backgrounds/3.png";
import bg4 from "../../../public/images/backgrounds/4.png";
import bg5 from "../../../public/images/backgrounds/5.png";
import bg6 from "../../../public/images/backgrounds/6.png";
import bg7 from "../../../public/images/backgrounds/7.png";
import bg8 from "../../../public/images/backgrounds/8.png";

//Backgrounds Fixos
import bgClassRom from "../../../public/images/backgrounds/class-rom-img.jpg";
import bgClassRom2 from "../../../public/images/backgrounds/class-rom-2-img.jpg";
import bgArena from "../../../public/images/backgrounds/arena-img.jpg";
import bgCubeDecision from "../../../public/images/backgrounds/cube-img.png";
import bgResultBattle from "../../../public/images/backgrounds/result-battle-img.png";
import bgFinal from "../../../public/images/backgrounds/final-img.png";

//Imagens
import speakImage from "../../../public/images/pictures/speak-img.png";
import muteSpeakImage from "../../../public/images/pictures/mute-speak-img.png";
import disketImage from "../../../public/images/pictures/save-img.png";
import exitImage from "../../../public/images/pictures/exit-img.png";
import minimizeImage from "../../../public/images/pictures/minimize-img.png";
import errorImage from "../../../public/images/pictures/error-img.png";

/**
 * Componente Responsável por prover o molde de sustentação dos menus no sistema.
 *
 */

@observer
@autobind
export default class CommonPage extends Component {
  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      background: [
        bg1,
        bg2,
        bg3,
        bg4,
        bg5,
        bg6,
        bg7,
        bg8,
        bgClassRom,
        bgClassRom2,
        bgArena,
        bgCubeDecision,
        bgResultBattle,
        bgFinal
      ],
      audio: [
        IntroMelody,
        MainMelody,
        Melody,
        FinalMelody,
        Melody2,
        Melody3,
        Melody4,
        RoundMelody,
        CubeDecisionMelody,
        ResultBattleMelody,
        FinalBattleMelody
      ],
      actualAudio: 0,
      currentBg: 8,
      currentAudio: -1,
      elementChild: {},
      deathStar: DeathStar.getInstance(React),
      bgInterval: "",
      hasOptions: false,
      componentReady: false
    };
  }

  /**
   * Realiza as ações quando o componente está sendo montado
   *
   * @param {Object} props
   * @param {Object} state
   */
  static getDerivedStateFromProps(props, state) {
    CommonPage.initComponents(props, state);
    CommonPage.setACurrentBackground(props, state);
    return CommonPage.handleOptionsByRoute(props, state);
  }

  /**
   * Executa uma ação sempre que o componente termina de ser montado.
   */
  componentDidMount() {
    this.state.componentReady = true;
  }

  /**
   * Cria o ciclo de mudanças do background
   *
   * @param {object} props
   * @param {object} state
   */
  static setBgInterval(props, state) {
    state.bgInterval = setInterval(() => {
      state.currentBg = state.currentBg == 7 ? 0 : ++state.currentBg % 8;
      document.body.setAttribute(
        "style",
        "background: url('" +
          state.background[state.currentBg] +
          "') no-repeat center center fixed"
      );
    }, (props.store.gincana.time / 8) * 1000);
  }

  /**
   * Responsável por fechar a aplicação.
   */
  closeAplication() {
    this.props.centralCommand.executeAction(
      "./ElectronBridge",
      "closeAplication"
    );
  }

  /**
   * Realiza as ações sempre que o componente acaba de ser atualizado
   *
   * @param {Object} prevProps
   * @param {Object} prevState
   */
  componentDidUpdate(prevProps, prevState) {
    CommonPage.setACurrentBackground(prevProps, prevState);
    document.getElementById("bgAudio").play();
  }

  /**
   * Inicia a tempestade na interface do sistema
   */
  static initStorm() {
    let storm = Storm.getInstance();
    storm.init();
    storm.animLoop();
  }

  /**
   * Retira a tesmpestade da interface do sistema.
   */
  static stopStorm() {
    Storm.getInstance().stopAnimLoop();
  }

  /**
   * Aplica as opções da página de acordo com a rota acessada
   *
   * @param {Object} props
   * @param {Object} state
   */
  static handleOptionsByRoute(props, state) {
    if (state.bgInterval) {
      clearInterval(state.bgInterval);
    }

    if (state.componentReady) {
      CommonPage.stopStorm();
    }

    state.hasOptions = true;

    switch (props.store.route) {
      case props.constants.WELCOME_KEY:
        state.currentBg = 8;
        state.actualAudio = 0;
        state.hasOptions = false;
        break;
      case props.constants.DATA_LOAD_KEY:
        state.currentBg = 9;
        state.hasOptions = false;
        break;
      case props.constants.RANKING_KEY:
        state.currentBg = 10;
        state.actualAudio = 1;
        state.hasOptions = false;
        break;
      case props.constants.BATTLE_INFO_KEY:
        //caso seja a rodada decisiva
        if (props.store.teamsData[props.store.battleLevel].length / 2 == 1) {
          CommonPage.initStorm();
          state.actualAudio = 4;
        } else {
          state.actualAudio = 2;
        }

        state.currentBg = 0;
        break;
      case props.constants.RESULT_BATTLE_KEY:
        state.actualAudio = 9;
        state.currentBg = 12;
        state.hasOptions = false;
        break;
      case props.constants.CUBE_DECISION_KEY:
        state.actualAudio = 8;
        state.currentBg = 11;
        break;
      case props.constants.FINAL_KEY:
        state.actualAudio = 3;
        state.currentBg = 13;
        break;
      case props.constants.BATTLE_QUEST_KEY:
        //caso seja a rodada decisiva
        if (props.store.teamsData[props.store.battleLevel].length / 2 == 1) {
          CommonPage.initStorm();
          state.actualAudio = 10;
        } else {
          state.actualAudio = 7;
        }

        state.currentBg = 0;
        CommonPage.setBgInterval(props, state);
        break;
    }

    return state;
  }

  /**
   * Atualiza os props  nos componentes mapeados pelo DeathStar
   */
  static updatePropsComponents(prevProps) {
    this.state.deathStar.setContext(
      this.props.constants.COMMON_PAGE_CONTEXT_KEY,
      this
    );
    this.state.deathStar
      .manipulate(this.props.constants.FINAL_KEY)
      .setProps(prevProps);
    this.state.deathStar
      .manipulate(this.props.constants.INTRO_KEY)
      .setProps(prevProps);
    this.state.deathStar
      .manipulate(this.props.constants.WELCOME_KEY)
      .setProps(prevProps);
    this.state.deathStar
      .manipulate(this.props.constants.RANKING_KEY)
      .setProps(prevProps);
    this.state.deathStar
      .manipulate(this.props.constants.DATA_LOAD_KEY)
      .setProps(prevProps);
    this.state.deathStar
      .manipulate(this.props.constants.BATTLE_INFO_KEY)
      .setProps(prevProps);
    this.state.deathStar
      .manipulate(this.props.constants.BATTLE_QUEST_KEY)
      .setProps(prevProps);
    this.state.deathStar
      .manipulate(this.props.constants.CUBE_DECISION_KEY)
      .setProps(prevProps);
    this.state.deathStar
      .manipulate(this.props.constants.RESULT_BATTLE_KEY)
      .setProps(prevProps);
    this.state.deathStar
      .manipulate(this.props.constants.RANKING_BALANCE_KEY)
      .setProps(prevProps);
  }

  /**
   * Inicializa os componentes no modulo do DeathStar
   *
   * @param {*} props
   * @param {*} state
   */
  static initComponents(props, state) {
    if (!state.deathStar.getElement(props.constants.WELCOME_KEY)) {
      let commonFunctions = {
        muteHandleFunction: CommonPage.muteHandleFunction,
        modalSaveHandleFunction: CommonPage.modalSaveHandleFunction,
        minimizeHandleFunction: CommonPage.minimizeHandleFunction,
        exitHandleFunction: CommonPage.exitHandleFunction
      };

      state.deathStar.processElement(
        <Welcome {...props} />,
        props.constants.WELCOME_KEY
      );
      state.deathStar.processElement(
        <FinalPage {...props} />,
        props.constants.FINAL_KEY
      );
      state.deathStar.processElement(
        <Ranking {...props} commonFunctions={commonFunctions} />,
        props.constants.RANKING_KEY
      );
      state.deathStar.processElement(
        <IntroPage {...props} commonFunctions={commonFunctions} />,
        props.constants.INTRO_KEY
      );
      state.deathStar.processElement(
        <DataLoad {...props} commonFunctions={commonFunctions} />,
        props.constants.DATA_LOAD_KEY
      );
      state.deathStar.processElement(
        <BattleInfo {...props} commonFunctions={commonFunctions} />,
        props.constants.BATTLE_INFO_KEY
      );
      state.deathStar.processElement(
        <BattleQuestion {...props} commonFunctions={commonFunctions} />,
        props.constants.BATTLE_QUEST_KEY
      );
      state.deathStar.processElement(
        <ResultBattle {...props} commonFunctions={commonFunctions} />,
        props.constants.RESULT_BATTLE_KEY
      );
      state.deathStar.processElement(
        <CubeDecision {...props} commonFunctions={commonFunctions} />,
        props.constants.CUBE_DECISION_KEY
      );
      state.deathStar.processElement(
        <RankingBalance {...props} commonFunctions={commonFunctions} />,
        props.constants.RANKING_BALANCE_KEY
      );
      state.deathStar.setContext(props.constants.COMMON_PAGE_CONTEXT_KEY, this);
    }
  }

  /**
   *  Seta o plano de fundo de acordo com o indice corrente.importação dos dados web
   *
   * @param {Object} props
   * @param {Object} state
   */
  static setACurrentBackground(props, state) {
    document.body.classList.add("fadeIn");
    document.body.setAttribute(
      "style",
      "background: url('" +
        state.background[state.currentBg] +
        "') no-repeat center center fixed"
    );

    let audioElement = document.getElementById("bgAudio");
    if (audioElement) {
      if (state.actualAudio != state.currentAudio) {
        audioElement.setAttribute("src", state.audio[state.actualAudio]);
        state.currentAudio = state.actualAudio;
      }
      audioElement.volume = props.store.volume;
    }

    if (props.store.customWidth) {
      let viewWidth = Math.max(
        document.documentElement.clientWidth,
        window.innerWidth || 0
      );

      if (props.store.customWidth > viewWidth) {
        document.body.style.width = props.store.customWidth + "px";
      }
    } else {
      document.body.style.width = "auto";
    }
  }

  /**
   * Especifica para os componentes internos se devem apresentar o conteudo ou não.
   *
   * @param {string} key
   */
  showElementRender(key) {
    let manipulateJSX = this.state.deathStar.manipulate(key);
    manipulateJSX.setAttribute({ renderElement: false });

    if (key == this.props.store.route) {
      manipulateJSX.setAttribute({ renderElement: true });
    }

    return manipulateJSX.getElement();
  }

  /**
   * Ativa e desativa a música no sistema.
   */
  static muteHandleFunction(props) {
    props.store.muted = !props.store.muted;
  }

  /**
   * Salva um snapshot da store no cookie do electron.
   */
  static saveHandleFunction(props) {
    props.centralCommand.executeAction(
      "./ElectronBridge",
      "saveDataInCookie",
      null,
      [props.labels]
    );
  }

  /**
   * Prepara o modal de confirmação da ação de salvar
   *
   * @param {object} props
   */
  static modalSaveHandleFunction(props) {
    props.store.modalMessage = props.labels.LABEL_QUESTION_SAVING;
    props.store.modalCancelable = true;
    props.store.modalImage = errorImage;

    props.store.modalFooter = (
      <Row className="rowFlex">
        <Col>
          <Button
            className="buttonOrange"
            onClick={CommonPage.saveHandleFunction.bind(this, props)}
            size="large"
          >
            <span className="fontVcrOsdMono" style={{ fontSize: "2em" }}>
              {props.labels.LABEL_OK}
            </span>
          </Button>
        </Col>
      </Row>
    );

    props.store.modal = true;
  }

  /**
   * Utiliza o handle do electron para minimizar a view.
   */
  static minimizeHandleFunction(props) {
    props.centralCommand.executeAction(
      "./ElectronBridge",
      "minimizeAplication"
    );
  }

  /**
   * Utiliza o handle do electron para dair da aplicação.
   */
  static exitHandleFunction(props) {
    props.centralCommand.executeAction("./ElectronBridge", "closeAplication");
  }

  /**
   * Retorna os elementos de opções para as páginas permitidas.
   */
  getOptionsElements() {
    let elementJSX = <div />;

    if (this.state.hasOptions) {
      elementJSX = (
        <div className="rowOptionsPages">
          <div className="options">
            <Tooltip
              placement="bottom"
              title={
                <div>
                  <p>{this.props.labels.LABEL_SOUND}</p>
                  <p>
                    {this.props.store.muted
                      ? this.props.labels.LABEL_OFF
                      : this.props.labels.LABEL_ON}
                  </p>
                </div>
              }
            >
              <Col
                onClick={CommonPage.muteHandleFunction.bind(this, this.props)}
              >
                <img
                  src={this.props.store.muted ? muteSpeakImage : speakImage}
                />
              </Col>
            </Tooltip>
            <Tooltip
              placement="bottom"
              title={
                <div>
                  <p>{this.props.labels.LABEL_SAVE}</p>
                </div>
              }
            >
              <Col
                onClick={CommonPage.modalSaveHandleFunction.bind(
                  this,
                  this.props
                )}
              >
                <img src={disketImage} />
              </Col>
            </Tooltip>
            <Tooltip
              placement="bottom"
              title={
                <div>
                  <p>{this.props.labels.LABEL_MINIMIZE}</p>
                </div>
              }
            >
              <Col
                onClick={CommonPage.minimizeHandleFunction.bind(
                  this,
                  this.props
                )}
              >
                <img src={minimizeImage} />
              </Col>
            </Tooltip>
            <Tooltip
              placement="bottom"
              title={
                <div>
                  <p>{this.props.labels.LABEL_EXIT}</p>
                </div>
              }
            >
              <Col onClick={this.closeAplication.bind(this, this.props)}>
                <img src={exitImage} />
              </Col>
            </Tooltip>
          </div>
        </div>
      );
    }

    return elementJSX;
  }

  /**
   * Renderiza o componente
   */
  render() {
    return (
      <div>
        <div className="stormThunder">
          <canvas id="canvas1"></canvas>
          <canvas id="canvas2"></canvas>
          <canvas id="canvas3"></canvas>
        </div>
        <LocaleProvider locale={pt_BR}>
          <div className="commonPage">
            {this.getOptionsElements()}
            <Modal
              visible={this.props.store.modal}
              className="modalBGColorInfo borderModal"
              closable={this.props.store.modalCancelable}
              maskClosable={this.props.store.modalCancelable}
              footer={this.props.store.modalFooter}
              onCancel={() => {
                this.props.store.modal = false;
              }}
            >
              <div className="modalVerify">
                <Row type="flex" justify="center" align="middle">
                  <Col>
                    <p className="fontVcrOsdMono verifyFontSize">
                      {this.props.store.modalMessage}
                    </p>
                  </Col>
                </Row>
                <Row type="flex" justify="center" align="middle">
                  <Col>
                    <img src={this.props.store.modalImage} />
                  </Col>
                </Row>
              </div>
            </Modal>
            {this.showElementRender(this.props.constants.WELCOME_KEY)}
            {this.showElementRender(this.props.constants.DATA_LOAD_KEY)}
            {this.showElementRender(this.props.constants.RANKING_KEY)}
            {this.showElementRender(this.props.constants.BATTLE_INFO_KEY)}
            {this.showElementRender(this.props.constants.BATTLE_QUEST_KEY)}
            {this.showElementRender(this.props.constants.RESULT_BATTLE_KEY)}
            {this.showElementRender(this.props.constants.CUBE_DECISION_KEY)}
            {this.showElementRender(this.props.constants.RANKING_BALANCE_KEY)}
            {this.showElementRender(this.props.constants.FINAL_KEY)}
            {this.showElementRender(this.props.constants.INTRO_KEY)}
          </div>
        </LocaleProvider>
        <audio
          loop
          id="bgAudio"
          muted={this.props.store.muted}
          autoPlay
        ></audio>
      </div>
    );
  }
}
