import React, { Component, } from 'react';
import { Row, Col, Button } from 'antd';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import '../../../public/css/final/final.css';

import numberOneImage from '../../../public/images/pictures/number-1-img.png';
import ripImage from '../../../public/images/pictures/rip-img.png';

/**
 * Componente Responsável por mostrar as opções iniciais
 * 
 */
@observer
@autobind
export default class FinalPage extends Component {


  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = FinalPage.initialState;
  }

  /**
   * Recupera o estado inicial do componente.
   */
  static get initialState() {
    return {
      initialise: false
    };
  }

  /**
   * Executado sempre que ocorre uma mudança no props ou ao montar o componente.
   * 
   * @param {object} props 
   * @param {object} state 
   */
  static getDerivedStateFromProps(props, state) {
    return FinalPage.initValuesOnRender(props, state);
  }

  /**
   * Inicializa as informações importantes para lançamento das questões
   * 
   */
  static initValuesOnRender(props, state) {
    if (props.renderElement && !state.initialise) {
      //INIT HERE
      state.initialise = true;
    } else if (!props.renderElement) {
      state = FinalPage.initialState;
    }
    return state;
  }

  /**
   * Recupera a linha que dita o ganahador da partida.
   */
  getWinnerRow() {
    let teamWinner = this.props.store.teams[this.props.store.teamsData.length - 1][0];

    let studentsCols = [];
    teamWinner.alunos.forEach((student, key) => {
      studentsCols.push(
        <Col key={key}>
          <span className="font8bitOperatorPlusBold">{student.nome}</span>
        </Col>
      )
    });

    return <Row className="rowFlex teamBlock">
      <Col>
        <Row className="rowColumn winnerSpan">
          <Col>
            <span className="fontKarmaFuture" style={{ fontSize: '4em' }}>{teamWinner.nome}</span>
          </Col>
          <Col>
            <img className="imgWinner" src={numberOneImage} />
          </Col>
        </Row>
      </Col>
      <Col>
        <Row className="rowColumn infoRow winnerSpan">
          <Col>
            <img src={"data:image/jpeg;" + teamWinner.image} />
          </Col>
          <Col>
            <span className="fontKarmaFuture">{this.props.labels.LABEL_VICTORIES}{teamWinner.victoriesNumber}</span>
          </Col>
          <Col>
            <span className="fontKarmaFuture">{this.props.labels.LABEL_STUDENTS}</span>
          </Col>
          <Col>
            <Row className="rowColumn">
              {studentsCols}
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>;
  }

  /**
   * Abre a tela de classificação das equipes
   */
  toWelcomePage() {
    document.getElementById('finalPage').className = 'goOutPage';
    setTimeout(() => { this.props.store.route = this.props.constants.WELCOME_KEY }, 1000);
  }

  /**
   * Recupera a linha que lista os derrotados da partida.
   */
  getLosersRow() {
    let teamWinner = this.props.store.teams[this.props.store.teamsData.length - 1][0];
    let teams = this.props.store.teamsData[0];
    let lostTeams = this.props.store.lostTeamsData;

    let allTeams = teams.concat(lostTeams);

    let auxFunctionOrder = function compare(teamA, teamB) {
      if (teamA.victoriesNumber > teamB.victoriesNumber)
        return -1;
      if (teamA.victoriesNumber < teamB.victoriesNumber)
        return 1;
      return 0;
    };

    allTeams = allTeams.sort(auxFunctionOrder);

    let teamsToPresentation = [];

    allTeams.forEach((team, keyTeam) => {
      if (team.nome != teamWinner.nome) {

        let studentsCols = [];
        team.alunos.forEach((student, keyStudent) => {
          studentsCols.push(
            <Col key={keyStudent}>
              <span className="font8bitOperatorPlusBold">{student.nome}</span>
            </Col>
          )
        });

        teamsToPresentation.push(
          <Row className="rowFlex teamBlock" key={keyTeam}>
            <Col>
              <Row className="rowColumn">
                <Col>
                  <span className="fontKarmaFuture" style={{ fontSize: '4em' }}>{team.nome}</span>
                </Col>
                <Col>
                  <img className="imgWinner" src={ripImage} />
                </Col>
              </Row>
            </Col>
            <Col>
              <Row className="rowColumn infoRow">
                <Col>
                  <img src={"data:image/jpeg;" + team.image} />
                </Col>
                <Col>
                  <span className="fontKarmaFuture">{this.props.labels.LABEL_VICTORIES}{team.victoriesNumber}</span>
                </Col>
                <Col>
                  <span className="fontKarmaFuture">{this.props.labels.LABEL_STUDENTS}</span>
                </Col>
                <Col>
                  <Row className="rowColumn">
                    {studentsCols}
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>);
      }
    });

    return teamsToPresentation;
  }

  /**
  * Renderiza o componente
  */
  render() {

    let elementJSX = <div />;
    if (this.props.renderElement && this.state.initialise) {
      elementJSX =
        <div className="goInPage teamsScore scrollbar" id="finalPage">
          <Row className="rowColumn">
            <Col>
              {this.getWinnerRow()}
              {this.getLosersRow()}
            </Col>
            <Col>
              <Button className="buttonYellow" onClick={this.toWelcomePage} style={{ margin: '40px' }} size='large'><span className="fontVcrOsdMono" style={{ fontSize: '2em' }}>{this.props.labels.LABEL_HOME}</span></Button>
            </Col>
          </Row>
        </div>;
    }

    return elementJSX;
  }
}