import React, { Component } from "react";
import { Row, Col, Tooltip } from "antd";
import autobind from "autobind-decorator";

import "../../../public/css/ranking/rankingItem.css";
import "../../../public/css/magic.css";
import trophyImage from "../../../public/images/pictures/trophy-img.png";
import questionImage from "../../../public/images/pictures/question-img.png";
import ripImage from "../../../public/images/pictures/rip-img.png";
import numberOneImage from "../../../public/images/pictures/number-1-img.png";
import DeathStar from "../../Lib/DeathStar";

/**
 * Componente Responsável por mostrar o ranking das equipes
 *
 */
@autobind
export default class RankingItemMap extends Component {
  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      deathStar: DeathStar.getInstance(React)
    };
  }

  /**
   * Ao clicar no item do map é dado o foco no item do ranking e fecha-se o modal.
   */
  focusItemRanking() {
    document
      .getElementById(this.props.targetFocus)
      .scrollIntoView({ behavior: "instant", block: "end", inline: "center" });
    let rankingContext = this.state.deathStar.getContext(
      this.props.constants.CONTEXT_RANKING_KEY
    );
    rankingContext.state.mapModal = !rankingContext.state.mapModal;
    rankingContext.update();
  }

  /**
   * Recupera o tipo de item que será apresentado de acordo com o estado da equipe informada nas propriedades do componente.
   */
  getElementRankingChild() {
    let elementRankingChild = {};
    if (this.props.team.winner === false) {
      elementRankingChild = (
        <Col onClick={this.focusItemRanking}>
          <img src={ripImage} />
        </Col>
      );
    } else if (this.props.team.winner) {
      elementRankingChild = (
        <Col onClick={this.focusItemRanking}>
          <img src={trophyImage} />
        </Col>
      );
    } else {
      elementRankingChild = (
        <Col onClick={this.focusItemRanking}>
          <img src={"data:image/jpeg;" + this.props.team.image} />
        </Col>
      );
    }

    return (
      <Tooltip placement="top" title={this.props.team.nome}>
        {elementRankingChild}
      </Tooltip>
    );
  }

  /**
   * Renderiza o componente
   */
  render() {
    let rankingItem = {};

    if (this.props.isSlot) {
      rankingItem = (
        <Row className="slotItemArea">
          <Col>
            {this.props.isSlotWinner ? (
              <img className="animatedShake" src={numberOneImage} />
            ) : (
              <img className="animatedShake" src={questionImage} />
            )}
          </Col>
        </Row>
      );
    } else {
      rankingItem = (
        <Row className="slotItemArea">
          <Row>{this.getElementRankingChild()}</Row>
        </Row>
      );
    }

    return <Col>{rankingItem}</Col>;
  }
}
