import React, { Component } from 'react';
import { Row, Col } from 'antd';
import autobind from 'autobind-decorator';

import '../../../public/css/ranking/rankingItem.css';
import '../../../public/css/magic.css';
import trophyImage from '../../../public/images/pictures/trophy-img.png';
import crownImage from '../../../public/images/pictures/crown-img.png';
import numberOneImage from '../../../public/images/pictures/number-1-img.png';
import ripImage from '../../../public/images/pictures/rip-img.png';

/**
 * Componente Responsável por mostrar o ranking das equipes
 * 
 */
@autobind
export default class RankingItem extends Component {

    /**
     * Construtor da classe
     */
    constructor() {
        super();
    }

    /**
      * Renderiza o componente
      */
    render() {
        let rankingItem = {};

        if (this.props.isSlot) {
            rankingItem = <Row className="slotArea">
                <Col>
                    {this.props.isSlotWinner ? <img className="animatedShake" src={numberOneImage} /> : <img className="animatedShake" src={crownImage} />}
                </Col>
            </Row>;
        } else {
            rankingItem = <Row className="hoverArea">
                <Row id={this.props.id} className={this.props.team.winner ? "blockInfoArea alignContent winer" : "blockInfoArea alignContent"}>
                    <Col>
                        <img src={this.props.team.winner === false ? ripImage : "data:image/jpeg;" + this.props.team.image} />
                    </Col>
                    <Row className="info">
                        <Col>
                            <p className="font8bitOperatorPlusSCBold">{this.props.team.nome}</p>
                        </Col>
                        <Col>
                            <p className="font8bitOperatorPlusSCBold">{this.props.team.message}</p>
                        </Col>
                    </Row>
                </Row>
                <Row className={this.props.team.winner ? "infoCardWinner" : "infoCard"}>
                    <Col>
                        <img src={trophyImage} />
                    </Col>
                    {this.props.team.winner ?
                        <div /> :
                        <Col>
                            <p className="font8bitOperatorPlusSCBold">{this.props.labels.LABEL_VICTORIES}</p>
                            <p className="font8bitOperatorPlusSCBold">{this.props.team.victoriesNumber}</p>
                        </Col>
                    }
                </Row>
            </Row>;
        }

        return (
            <Col>
                {rankingItem}
            </Col>
        );
    }
}
