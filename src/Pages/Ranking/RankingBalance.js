import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import { Row, Col, Modal, Button } from 'antd';

import '../../../public/css/ranking/ranking.css';

import BalanceImage from '../../../public/images/pictures/balance-img.png';
import vsBattleImage from '../../../public/images/pictures/vs-img.png';

/**
 * Componente Responsável por mostrar o ranking das equipes
 * 
 */
@autobind
export default class RankingBalance extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = RankingBalance.initialState;
  }

  /**
 * Executado sempre que ocorre uma mudança no props ou ao montar o componente.
 * 
 * @param {object} props 
 * @param {object} state 
 */
  static getDerivedStateFromProps(props, state) {
    return RankingBalance.initValuesOnRender(props, state);
  }

  /**
  * Recupera o estado inicial do componente.
  */
  static get initialState() {
    return {
      initialise: false,
      battleModal: false
    };
  }

  /**
 * Iniciar todas as variaveis no momento em que o conteudo do render deve ser mostrado.
 */
  static initValuesOnRender(props, state) {
    if (props.renderElement && !state.initialise) {
      state = RankingBalance.initialState;

      props.centralCommand.executeAction('./GincanaController', 'getNextBattleBalance', [], [props.labels]);

      state.initialise = true;
    } else if (!props.renderElement) {
      state = RankingBalance.initialState;
    }

    return state;
  }

  /**
   * Manipulador que controla a abertura do modal de batalhas
   */
  mapBattleModalHandle() {
    this.setState({
      battleModal: !this.state.battleModal
    });
  }

  /**
 * Abre a tela de questões da batalha
 */
  toBattleInfoPage() {
    this.mapBattleModalHandle();
    document.getElementById('balance').className = 'goOutPage';
    //reseta as etapas da rodada
    this.props.store.nextBattle.step = 1;

    setTimeout(() => { this.props.store.route = this.props.constants.BATTLE_INFO_KEY }, 1500);
  }


  /**
   * Recupera o componente do modal de proxima batalha para renderização
   */
  getNextBattleModalComponent() {
    let battleTeam1 = this.props.store.teamsData[this.props.store.battleLevel][this.props.store.nextBattle.team.index];
    let battleTeam2 = this.props.store.teamsData[this.props.store.battleLevel][this.props.store.nextBattle.teamVs.index];

    return (
      <Modal
        visible={this.state.battleModal}
        onCancel={this.mapBattleModalHandle}
        maskClosable={true}
        closable={false}
        mask={true}
        className="battleModal"
        footer={null}
        width="70vw"
      >
        <Row className="rowFlexBattle">
          <Col>
            <span className="fontKarmaFuture" style={{ fontSize: '4em', margin: '10px' }}>{this.props.labels.LABEL_BATTLE_INFO}</span>
          </Col>
        </Row>
        <Row className="rowFlexBattle">
          <Col style={{ width: "40.5%" }}>
            <Row>
              <Row className="rowFlex">
                <img className="imgTeamBattleInfo" src={"data:image/jpeg;" + battleTeam1.image} />
              </Row>
              <Col>
                <Row className="teamBattleInfo">
                  <Col>
                    <p className="font8bitOperatorPlusSCBold">{battleTeam1.nome}</p>
                  </Col>
                  <Col>
                    <p className="font8bitOperatorPlusSCBold">{battleTeam1.message}</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col style={{ width: "15%" }}>
            <img className="imgTeamBattleVS" src={vsBattleImage} />
          </Col>
          <Col style={{ width: "40.5%" }}>
            <Row>
              <Row className="rowFlex">
                <img className="imgTeamBattleInfo" src={"data:image/jpeg;" + battleTeam2.image} />
              </Row>
              <Col>
                <Row className="teamBattleInfo">
                  <Col>
                    <p className="font8bitOperatorPlusSCBold">{battleTeam2.nome}</p>
                  </Col>
                  <Col>
                    <p className="font8bitOperatorPlusSCBold">{battleTeam2.message}</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="rowFlexBattle">
          <Col>
            <Button className="buttonOrange" onClick={this.mapBattleModalHandle} size='large'><span className="fontVcrOsdMono" style={{ fontSize: '2em' }}>{this.props.labels.LABEL_BACK}</span></Button>
          </Col>
          <Col>
            <Button className="buttonYellow" onClick={this.toBattleInfoPage} size='large'><span className="fontVcrOsdMono" style={{ fontSize: '2em' }}>{this.props.labels.LABEL_START}</span></Button>
          </Col>
        </Row>
      </Modal>
    )
  }

  /**
  * Renderiza o componente
  */
  render() {
    //variveis observadas da store

    let elementJSX = <div />;
    if (this.props.renderElement && this.state.initialise) {
      elementJSX =
        <div className="goInPage" id="balance">
          {this.getNextBattleModalComponent()}
          <Row className="rowFlex borderFrame">
            <Col className="rowColumn spaceAroundColumn">
              <Col>
                <h1 className="font8bitOperatorPlusSCBold">{this.props.labels.LABEL_BALANCE_INFO}</h1>
              </Col>
              <Col>
                <img className="imgTeamBattleInfo" src={BalanceImage} />
              </Col>
              <Col>
                <h1 className="font8bitOperatorPlusSCBold">{this.props.labels.LABEL_BALANCE_TIMES_INFO[0]} <span className="fontVcrOsdMono" style={{ fontSize: '3em', color: 'yellow' }}>{this.props.store.balancingTimes}</span> {this.props.labels.LABEL_BALANCE_TIMES_INFO[1]}</h1>
              </Col>
              <Col>
                <Button className="buttonOrange" onClick={this.mapBattleModalHandle} size='large'><span className="fontVcrOsdMono" style={{ fontSize: '2em' }}>{this.props.labels.LABEL_START}</span></Button>
              </Col>
            </Col>
          </Row>
        </div>
    }

    return elementJSX;
  }
}