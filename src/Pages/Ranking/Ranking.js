import React, { Component } from "react";
import { Row, Col, Tooltip, Modal, Button } from "antd";
import autobind from "autobind-decorator";
import { observer } from "mobx-react";
import RankingItem from "./RankingItem";
import RankingItemMap from "./RankingItemMap";

import DeathStar from "../../Lib/DeathStar";

import "../../../public/css/ranking/ranking.css";
import "../../../public/css/battle/battle.css";

import mapImage from "../../../public/images/pictures/map-img.png";
import vsBattleImage from "../../../public/images/pictures/vs-img.png";
import speakImage from "../../../public/images/pictures/speak-img.png";
import startImage from "../../../public/images/pictures/start-img.png";
import muteSpeakImage from "../../../public/images/pictures/mute-speak-img.png";
import mapBackgroundImage from "../../../public/images/backgrounds/map-background-img.png";
import disketImage from "../../../public/images/pictures/save-img.png";
import exitImage from "../../../public/images/pictures/exit-img.png";
import minimizeImage from "../../../public/images/pictures/minimize-img.png";
/**
 * Componente Responsável por mostrar o ranking das equipes
 *
 */
@observer
@autobind
export default class Ranking extends Component {
  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = Ranking.initialState;
  }

  /**
   * Recupera o estado inicial do componente.
   */
  static get initialState() {
    return {
      scroll: false,
      maxScrollHeight: 0,
      maxScrollWidth: 0,
      scrollHeight: window.pageYOffset,
      scrollWidth: window.pageXOffset,
      hoverScroll: "",
      deathStar: DeathStar.getInstance(React),
      mapModal: false,
      battleModal: false,
      initialise: false
    };
  }

  /**
   * Cria a lista de ranking com o estado inicial das pontuações.
   *
   * @param {object} props
   * @param {object} state
   */
  static createRankingList(props, state) {
    state.deathStar.processElement(<div />, props.constants.TEAM_KEY);
    let numberOfSlots = props.store.teamsSize;
    while (numberOfSlots >= 1) {
      numberOfSlots = Ranking.createRankinSlot(numberOfSlots, props, state);
    }
  }

  /**
   * Cria a lista de itens do ranking para que sejam mostradas no mapa.
   *
   * @param {object} props
   * @param {object} state
   */
  static createItemRankingMapItemList(props, state) {
    state.deathStar.processElement(<div />, props.constants.TEAM_MAP_ITENS_KEY);
    let numberOfSlots = props.store.teamsSize;
    while (numberOfSlots >= 1) {
      numberOfSlots = Ranking.createRankinMapItemSlot(
        numberOfSlots,
        props,
        state
      );
    }
  }

  /**
   * Cria os demais slots vazios que compoem o grafico do ranking mostrado no mapa.
   *
   * @param {number} teamNumber
   * @param {object} props
   * @param {object} state
   */
  static createRankinMapItemSlot(teamNumber, props, state) {
    let teamsElements = [];
    for (var key = 0; key < teamNumber; key++) {
      teamsElements.push(
        <RankingItemMap
          isSlot="true"
          key={props.store.getId()}
          isSlotWinner={teamNumber == 1}
          {...props}
        />
      );
    }

    state.deathStar
      .manipulate(props.constants.TEAM_MAP_ITENS_KEY)
      .setChildren(<Row className="rowFlex">{teamsElements}</Row>);

    return teamNumber / 2;
  }

  /**
   * Cria uma descrição que divide o mapa de equipes da classificação corrente.
   *
   * @param {object} props
   * @param {object} state
   */
  static createDividerRanking(props, state) {
    let itensMapRanking = state.deathStar.manipulate(
      props.constants.TEAM_MAP_ITENS_KEY
    );

    if (itensMapRanking) {
      itensMapRanking.setChildren(
        <Row className="rowFlex">
          <Col>
            <h1 className="font8bitOperatorPlusSCBold">
              {props.labels.LABEL_TEAM_MAP_RANKING}
            </h1>
          </Col>
        </Row>,
        2,
        true
      );
    }
  }

  /**
   * Atualiza os itens do Ranking relacionados ao mapa utilizando o array de equipes disponíveis.
   *
   * @param {object} props
   * @param {object} state
   */
  static updateRankingMapItens(props, state) {
    props.store.teamsData.forEach((teams, keyLevel) => {
      ++keyLevel;
      if (keyLevel == 1) {
        state.deathStar
          .manipulate(props.constants.TEAM_MAP_ITENS_KEY + "-Row" + keyLevel)
          .setAttribute({ className: "ant-row rowFlex wrapRow" });
      }

      teams.forEach((team, key) => {
        ++key;
        let targetKey =
          props.constants.TEAM_KEY + "-Row" + keyLevel + "-RankingItem" + key;
        let manipulateElement = state.deathStar.manipulate(
          props.constants.TEAM_MAP_ITENS_KEY +
            "-Row" +
            keyLevel +
            "-RankingItemMap" +
            key
        );
        manipulateElement
          .setAttribute({ team: team, targetFocus: targetKey })
          .removeAttribute("isSlot");
      });
    });
  }

  /**
   * Atualiza os itens do Ranking por meio do array de equipes disponíveis.
   *
   * @param {object} props
   * @param {object} state
   */
  static updateRankingItens(props, state) {
    props.store.teamsData.forEach((teams, keyLevel) => {
      ++keyLevel;
      teams.forEach((team, key) => {
        let targetKey =
          props.constants.TEAM_KEY + "-Row" + keyLevel + "-RankingItem" + ++key;
        let manipulateElement = state.deathStar.manipulate(targetKey);
        manipulateElement
          .setAttribute({ team: team, id: targetKey })
          .removeAttribute("isSlot");
      });
    });
  }

  /**
   * Cria os demais slots vazios que compoem o grafico do ranking
   *
   * @param {number} teamNumber
   * @param {object} props
   * @param {object} state
   */
  static createRankinSlot(teamNumber, props, state) {
    let teamsElements = [];
    for (var key = 0; key < teamNumber; key++) {
      teamsElements.push(
        <RankingItem
          isSlot="true"
          key={props.store.getId()}
          isSlotWinner={teamNumber == 1}
          {...props}
        />
      );
    }

    state.deathStar
      .manipulate(props.constants.TEAM_KEY)
      .setChildren(<Row className="rowFlex">{teamsElements}</Row>);

    return teamNumber / 2;
  }

  /**
   * Para a leitura do manipulador de posição do scroll
   */
  stopScroll() {
    clearInterval(this.state.hoverScroll);
  }

  /**
   * Executado sempre que ocorre uma mudança no props ou ao montar o componente.
   *
   * @param {object} props
   * @param {object} state
   */
  static getDerivedStateFromProps(props, state) {
    return Ranking.initValuesOnRender(props, state);
  }

  /**
   * Executa ações enquanto o componente atualiza
   */
  getSnapshotBeforeUpdate() {
    this.state.deathStar.setContext(
      this.props.constants.CONTEXT_RANKING_KEY,
      this
    );
    return null;
  }

  /**
   * Executa ações quando o componente termina de ser atualizado
   */
  componentDidMount() {
    this.state.deathStar.setContext(
      this.props.constants.CONTEXT_RANKING_KEY,
      this
    );
  }

  /**
   * Move o scrool da tela para a direção informada.
   *
   * @param {string} target
   */
  moveScroll(target) {
    switch (target) {
      case "L":
        document.documentElement.scrollLeft =
          document.documentElement.scrollLeft - 20;
        break;
      case "R":
        document.documentElement.scrollLeft =
          document.documentElement.scrollLeft + 20;
        break;
      case "U":
        document.documentElement.scrollTop =
          document.documentElement.scrollTop - 20;
        break;
      case "D":
        document.documentElement.scrollTop =
          document.documentElement.scrollTop + 20;
        break;
    }
  }

  /**
   * Manipulador que controla a abertura do modal do mapa
   */
  mapModalHandle() {
    this.setState({
      mapModal: !this.state.mapModal
    });
  }

  /**
   * Manipulador que controla a abertura do modal de batalhas
   */
  mapBattleModalHandle() {
    this.setState({
      battleModal: !this.state.battleModal
    });
  }

  /**
   * Dispara o manipulador de movimento do scroll de acordo com a direção informada.
   *
   * @param {string} target
   */
  handleScroll(target) {
    this.state.hoverScroll = setInterval(() => {
      this.moveScroll(target);
    }, 20);
  }

  /**
   * Abre a tela de questões da batalha
   */
  toBattleInfoPage() {
    this.mapBattleModalHandle();
    document.getElementById("ranking").className = "goOutPage";
    this.props.store.customWidth = "";
    //reseta as etapas da rodada
    this.props.store.nextBattle.step = 1;

    setTimeout(() => {
      this.props.store.route = this.props.constants.BATTLE_INFO_KEY;
    }, 1500);
  }

  /**
   * Iniciar todas as variaveis no momento em que o conteudo do render deve ser mostrado.
   */
  static initValuesOnRender(props, state) {
    if (props.renderElement && !state.initialise) {
      state = Ranking.initialState;
      props.centralCommand.executeAction(
        "./GincanaController",
        "getNextBattle",
        [],
        [props.labels]
      );

      Ranking.createRankingList(props, state);
      Ranking.updateRankingItens(props, state);
      Ranking.createItemRankingMapItemList(props, state);
      Ranking.updateRankingMapItens(props, state);
      Ranking.createDividerRanking(props, state);
      //Não alterar a ordem da chamada destas funções

      state.maxScrollHeight = document.documentElement.scrollHeight;
      state.maxScrollWidth = props.store.customWidth;

      state.initialise = true;
      //Por meio da gincana importada seta-se o tamanho do corpo da página de ranking para navegação
      props.store.customWidth =
        props.store.teamsSize * props.constants.TEAM_ELEMENTS_SIZE;
    }
    if (!props.renderElement) {
      state = Ranking.initialState;
    }

    return state;
  }

  /**
   * Recupera o componente do modal de mapa do ranking para renderização
   */
  getMapModalComponent() {
    let rankingMapElements = this.state.deathStar.getElement(
      this.props.constants.TEAM_MAP_ITENS_KEY
    );

    return (
      <Modal
        visible={this.state.mapModal}
        onCancel={this.mapModalHandle}
        maskClosable={true}
        closable={false}
        mask={false}
        footer={null}
        width="auto"
        className="mapModal"
        bodyStyle={{
          height: "98vh",
          background:
            "url('" + mapBackgroundImage + "') no-repeat center center fixed"
        }}
      >
        <Row className="rowColumn">
          <Row type="flex" justify="center" className="mapContent">
            <Col>
              <h1 className="font8bitOperatorPlusSCBold">
                {this.props.labels.LABEL_EVENT}
              </h1>
              <span className="fontKarmaFuture">
                {this.props.store.gincanaData.nome}
              </span>
              <h1 className="font8bitOperatorPlusSCBold">
                {this.props.labels.LABEL_EVENT_THEME}
              </h1>
              <span className="fontKarmaFuture">
                {this.props.store.gincanaData.tema}
              </span>
            </Col>
          </Row>
          <div className="mapRanking">
            <Row type="flex" justify="center">
              <Col>
                <h1 className="font8bitOperatorPlusSCBold">
                  {this.props.labels.LABEL_TEAM_MAP}
                </h1>
              </Col>
            </Row>
            <Row>
              <Col>{rankingMapElements ? rankingMapElements : <div />}</Col>
            </Row>
          </div>
        </Row>
      </Modal>
    );
  }

  /**
   * Recupera o componente do modal de proxima batalha para renderização
   */
  getNextBattleModalComponent() {
    let battleTeam1 = this.props.store.teamsData[this.props.store.battleLevel][
      this.props.store.nextBattle.team.index
    ];
    let battleTeam2 = this.props.store.teamsData[this.props.store.battleLevel][
      this.props.store.nextBattle.teamVs.index
    ];

    let lastBattle =
      this.props.store.teamsData[this.props.store.battleLevel].length / 2 == 1;
    return (
      <Modal
        visible={this.state.battleModal}
        onCancel={this.mapBattleModalHandle}
        maskClosable={true}
        closable={false}
        mask={true}
        className="battleModal"
        footer={null}
        width="70vw"
      >
        <Row className="rowFlexBattle">
          <Col>
            <span
              className="fontKarmaFuture"
              style={{ fontSize: "4em", margin: "10px" }}
            >
              {lastBattle
                ? this.props.labels.LABEL_LAST_BATTLE
                : this.props.labels.LABEL_BATTLE_INFO}
            </span>
          </Col>
        </Row>
        <Row className="rowFlexBattle">
          <Col style={{ width: "40.5%" }}>
            <Row>
              <Row className="rowFlex">
                <img
                  className="imgTeamBattleInfo"
                  src={"data:image/jpeg;" + battleTeam1.image}
                />
              </Row>
              <Col>
                <Row className="teamBattleInfo">
                  <Col>
                    <p className="font8bitOperatorPlusSCBold">
                      {battleTeam1.nome}
                    </p>
                  </Col>
                  <Col>
                    <p className="font8bitOperatorPlusSCBold">
                      {battleTeam1.message}
                    </p>
                  </Col>
                  <Col>
                    <p className="font8bitOperatorPlusSCBold">
                      {this.props.labels.LABEL_VICTORIES}
                      {battleTeam1.victoriesNumber}
                    </p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col style={{ width: "15%" }}>
            <img className="imgTeamBattleVS" src={vsBattleImage} />
          </Col>
          <Col style={{ width: "40.5%" }}>
            <Row>
              <Row className="rowFlex">
                <img
                  className="imgTeamBattleInfo"
                  src={"data:image/jpeg;" + battleTeam2.image}
                />
              </Row>
              <Col>
                <Row className="teamBattleInfo">
                  <Col>
                    <p className="font8bitOperatorPlusSCBold">
                      {battleTeam2.nome}
                    </p>
                  </Col>
                  <Col>
                    <p className="font8bitOperatorPlusSCBold">
                      {battleTeam2.message}
                    </p>
                  </Col>
                  <Col>
                    <p className="font8bitOperatorPlusSCBold">
                      {this.props.labels.LABEL_VICTORIES}
                      {battleTeam2.victoriesNumber}
                    </p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="rowFlexBattle">
          <Col>
            <Button
              className="buttonOrange"
              onClick={this.mapBattleModalHandle}
              size="large"
            >
              <span className="fontVcrOsdMono" style={{ fontSize: "2em" }}>
                {this.props.labels.LABEL_BACK}
              </span>
            </Button>
          </Col>
          <Col>
            <Button
              className="buttonYellow"
              onClick={this.toBattleInfoPage}
              size="large"
            >
              <span className="fontVcrOsdMono" style={{ fontSize: "2em" }}>
                {this.props.labels.LABEL_START}
              </span>
            </Button>
          </Col>
        </Row>
      </Modal>
    );
  }

  /**
   * Renderiza o componente
   */
  render() {
    //variveis observadas da store

    let elementJSX = <div />;
    if (this.props.renderElement && this.state.initialise) {
      let rankingElements = this.state.deathStar.getElement(
        this.props.constants.TEAM_KEY
      );

      //calculo de tamanho linha com numero maior de itens "itens"x"500" aplicar width no content
      elementJSX = (
        <div className="goInPage" id="ranking">
          {this.getNextBattleModalComponent()}
          {this.getMapModalComponent()}

          {/* Conteúdo da página */}
          <div className="rowOptions">
            <div className="options">
              <Tooltip
                placement="bottom"
                title={
                  <div>
                    <p>{this.props.labels.LABEL_SOUND}</p>
                    <p>
                      {this.props.store.muted
                        ? this.props.labels.LABEL_OFF
                        : this.props.labels.LABEL_ON}
                    </p>
                  </div>
                }
              >
                <Col
                  onClick={this.props.commonFunctions.muteHandleFunction.bind(
                    this,
                    this.props
                  )}
                >
                  <img
                    src={this.props.store.muted ? muteSpeakImage : speakImage}
                  />
                </Col>
              </Tooltip>
              <Tooltip
                placement="bottom"
                title={this.props.labels.LABEL_OPEN_MAP}
              >
                <Col onClick={this.mapModalHandle}>
                  <img src={mapImage} />
                </Col>
              </Tooltip>
              <Tooltip
                placement="bottom"
                title={
                  <div>
                    <p>{this.props.labels.LABEL_NEXT}</p>
                    <p>{this.props.labels.LABEL_BATTLE}</p>
                  </div>
                }
              >
                <Col onClick={this.mapBattleModalHandle}>
                  <img src={startImage} />
                </Col>
              </Tooltip>
              <Tooltip
                placement="bottom"
                title={
                  <div>
                    <p>{this.props.labels.LABEL_SAVE}</p>
                  </div>
                }
              >
                <Col
                  onClick={this.props.commonFunctions.modalSaveHandleFunction.bind(
                    this,
                    this.props
                  )}
                >
                  <img src={disketImage} />
                </Col>
              </Tooltip>
              <Tooltip
                placement="bottom"
                title={
                  <div>
                    <p>{this.props.labels.LABEL_MINIMIZE}</p>
                  </div>
                }
              >
                <Col
                  onClick={this.props.commonFunctions.minimizeHandleFunction.bind(
                    this,
                    this.props
                  )}
                >
                  <img src={minimizeImage} />
                </Col>
              </Tooltip>
              <Tooltip
                placement="bottom"
                title={
                  <div>
                    <p>{this.props.labels.LABEL_EXIT}</p>
                  </div>
                }
              >
                <Col
                  onClick={this.props.commonFunctions.exitHandleFunction.bind(
                    this,
                    this.props
                  )}
                >
                  <img src={exitImage} />
                </Col>
              </Tooltip>
            </div>
          </div>
          <div className="navBar">
            <Row
              className="navLeft arrowsCol hoverSolid"
              onMouseEnter={this.handleScroll.bind(this, "L")}
              onMouseLeave={this.stopScroll}
            >
              <div className="arrowsColContent border-arrow-left"></div>
              <Row className="navLeft arrowsCol">
                <div className="arrowsColContent arrow-left"></div>
              </Row>
            </Row>
            <Row
              className="navRigth arrowsCol hoverSolid"
              onMouseEnter={this.handleScroll.bind(this, "R")}
              onMouseLeave={this.stopScroll}
            >
              <div className="arrowsColContent border-arrow-right"></div>
              <Row className="navRigth arrowsCol">
                <div className="arrowsColContent arrow-right"></div>
              </Row>
            </Row>
            <Row
              className="navUp arrowsRow hoverSolid"
              onMouseEnter={this.handleScroll.bind(this, "U")}
              onMouseLeave={this.stopScroll}
            >
              <div className="arrowsRowContent border-arrow-up"></div>
              <Row className="navUp arrowsRow">
                <div className="arrowsRowContent arrow-up"></div>
              </Row>
            </Row>
            <Row
              className="navDown arrowsRow hoverSolid"
              onMouseEnter={this.handleScroll.bind(this, "D")}
              onMouseLeave={this.stopScroll}
            >
              <div className="arrowsRowContent border-arrow-down"></div>
              <Row className="navDown arrowsRow">
                <div className="arrowsRowContent arrow-down"></div>
              </Row>
            </Row>
          </div>
          <div className="rankingGraph">
            {rankingElements ? rankingElements : <div />}
          </div>
        </div>
      );
    }

    return elementJSX;
  }
}
