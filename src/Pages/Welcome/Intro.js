import React, { Component, Image } from 'react';
import { Row, Col, Layout, Button } from 'antd';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import '../../../public/css/welcome/welcome.css';

import bookImage from '../../../public/images/pictures/book-8-bits.png';

const { Header, Content } = Layout;

/**
 * Componente Responsável por mostrar as opções iniciais
 * 
 */
@observer
@autobind
export default class Intro extends Component {

    /**
     * Construtor da classe
     */
    constructor() {
        super();
    }

    /**
     * Abre a tela de classificação das equipes
     */
    toPage(target) {
        document.getElementById('intro').className = 'goOutPage borderFrame';
        setTimeout(() => { this.props.store.route = target }, 1000);
    }

    /**
     * Iniciar o fluxo da gincana redirecionando para o balaceamento ou ranking.
     * 
     * @param {Object} file 
     */
    startGame() {
        this.props.centralCommand.executeAction('./GincanaController', 'startGame', [this.toPage], [this.props.labels]);
    }

    /**
      * Renderiza o componente
      */
    render() {
        //variáveis observadas da store.
        this.props.store.teamsSize;

        let elementJSX = <div />;
        if (this.props.renderElement) {

            elementJSX =
                <div className="goInPage borderFrame rowColumn" id="intro">
                    <Row type="flex" justify="center" className="intro">
                        <Col>
                            <h1 className="font8bitOperatorPlusSCBold">{this.props.labels.LABEL_EVENT}</h1>
                            <span className="fontKarmaFuture">{this.props.store.gincanaData.nome}</span>
                            <h1 className="font8bitOperatorPlusSCBold">{this.props.labels.LABEL_EVENT_THEME}</h1>
                            <span className="fontKarmaFuture">{this.props.store.gincanaData.tema}</span>
                            <h1 className="font8bitOperatorPlusSCBold">{this.props.labels.LABEL_TEACHER_RESPON}</h1>
                            <span className="fontKarmaFuture">{this.props.store.gincanaData.professor}</span>
                        </Col>
                    </Row>
                    <Row type='flex' justify='center' className="buttonPosition">
                        <Col>
                            <Button className="buttonYellow" size='large' onClick={this.startGame}><span className="fontVcrOsdMono" style={{ fontSize: '2em' }}>{this.props.labels.LABEL_START}</span></Button>
                        </Col>
                    </Row>
                </div>;
        }

        return elementJSX;
    }
}
