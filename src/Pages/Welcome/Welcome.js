import React, { Component, Image } from 'react';
import { Row, Col, Layout, Button } from 'antd';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import '../../../public/css/welcome/welcome.css';
import packageJson from '../../../package.json';

import bookImage from '../../../public/images/pictures/book-8-bits.png';

const { Header, Content } = Layout;

/**
 * Componente Responsável por mostrar as opções iniciais
 * 
 */
@observer
@autobind
export default class Welcome extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
  }

  /**
   * Executado sempre que ocorre uma mudança no props ou ao montar o componente.
   * 
   * @param {object} props 
   * @param {object} state 
   */
  static getDerivedStateFromProps(props, state) {
    if (props.renderElement) {
      //iniciar sempre um novo contexto de batalha
      props.store.resetStore();
    }

    return null;
  }

  /**
   * Abre a tela de carregamento de dados
   */
  toDataLoadPage() {
    document.getElementById('welcome').className = 'goOutPage';
    setTimeout(() => { this.props.store.route = this.props.constants.DATA_LOAD_KEY }, 1000);
  }

  /**
    * Renderiza o componente
    */
  render() {
    //variveis observadas da store

    let elementJSX = <div />;
    if (this.props.renderElement) {

      elementJSX =
        <div id='welcome'>
          <Row type='flex' className='titleAppName' justify="center" align="middle">
            <Col>
              <img src={bookImage} />
            </Col>
            <Col>
              <span className="fontKarmaFuture extendedShadows" style={{ fontSize: '11em' }}>{this.props.labels.TITLE_APP_NAME}</span>
            </Col>
            <Col>
              <span className="fontKarmaFuture extendedShadows" style={{ fontSize: '2em' }}>V {packageJson.version}</span>
            </Col>
          </Row>
          <Row type='flex' justify='center' className="centerOfScreen">
            <Col>
              <span className="fontVcrOsdMono extendedShadows" style={{ fontSize: '5em' }}>{this.props.labels.TITLE_WELCOME}</span>
            </Col>
          </Row>
          <Row type='flex' justify='center' className="buttonPosition">
            <Col>
              <Button className="buttonYellow" size='large' onClick={this.toDataLoadPage}><span className="fontVcrOsdMono" style={{ fontSize: '2em' }}>{this.props.labels.LABEL_START}</span></Button>
            </Col>
          </Row>
        </div>;
    }

    return elementJSX;
  }
}
