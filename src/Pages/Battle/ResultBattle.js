import React, { Component, } from 'react';
import { Row, Col, Button } from 'antd';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";

import crownImage from '../../../public/images/pictures/crown-img.png';

/**
 * Componente Responsável por mostrar as opções iniciais
 * 
 */
@observer
@autobind
export default class ResultBattle extends Component {


  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = ResultBattle.initialState;
  }

  /**
   * Recupera o estado inicial do componente.
   */
  static get initialState() {
    return {
      initialise: false
    };
  }

  /**
   * Executado sempre que ocorre uma mudança no props ou ao montar o componente.
   * 
   * @param {object} props 
   * @param {object} state 
   */
  static getDerivedStateFromProps(props, state) {
    return ResultBattle.initValuesOnRender(props, state);
  }

  /**
   * Inicializa as informações importantes para lançamento das questões
   * 
   */
  static initValuesOnRender(props, state) {
    if (props.renderElement && !state.initialise) {
      //INIT HERE
      state.isFinal = props.store.teamsData[props.store.battleLevel].length / 2 == 1;
      state.initialise = true;
    } else if (!props.renderElement) {
      state = ResultBattle.initialState;
    }
    return state;
  }

  /**
   * Abre a tela de classificação das equipes
   */
  toRankingPage() {
    //reseta a batalha que acabou
    this.props.store.resetNextBattle();

    //atualiza o indice de batalhas ocorridas
    ++this.props.store.occurredBattles;
    document.getElementById('resultBattle').className = 'goOutPage';
    setTimeout(() => { this.props.store.route = this.props.constants.RANKING_KEY }, 1000);
  }

  /**
   * Abre a tela de classificação das equipes
   */
  toFinalPage() {
    //reseta a batalha que acabou
    this.props.store.resetNextBattle();
    document.getElementById('resultBattle').className = 'goOutPage';
    setTimeout(() => { this.props.store.route = this.props.constants.FINAL_KEY }, 1000);
  }

  /**
 * Abre a tela de classificação das equipes
 */
  toRankingBalancePage() {
    //reseta a batalha que acabou
    this.props.store.resetNextBattle();
    
    document.getElementById('resultBattle').className = 'goOutPage';
    setTimeout(() => { this.props.store.route = this.props.constants.RANKING_BALANCE_KEY }, 1000);
  }

  /**
  * Renderiza o componente
  */
  render() {
    //variveis observadas da store

    let elementJSX = <div />;
    if (this.props.renderElement && this.state.initialise) {
      let team1 = this.props.store.teamsData[this.props.store.battleLevel][this.props.store.nextBattle.team.index];
      let team2 = this.props.store.teamsData[this.props.store.battleLevel][this.props.store.nextBattle.teamVs.index];

      let teamWinner = team1.winner ? team1 : team2;
      elementJSX =
        <div className="goInPage" id="resultBattle">
          <Row className="borderFrame">
            <Col>
              <Row className="rowFlexBattle">
                <Col>
                  <span className="fontKarmaFuture" style={{ fontSize: '5em', margin: '10px' }}>{this.props.labels.LABEL_BATTLE_RESULTS}</span>
                </Col>
              </Row>
            </Col>
            <Col>
              <Row className="rowFlexBattle">
                <Col className="cubeDecisionTeams">
                  <Row className="rowColumn">
                    <Col className="colTeamBattleCrown">
                      <img className="imgTeamBattleCrown" src={crownImage} />
                    </Col>
                    <Col className="animateVanishOut">
                      <img className="imgTeamWinner" src={"data:image/jpeg;" + teamWinner.image} />
                    </Col>
                    <Col>
                      <Row className="teamBattleInfo">
                        <Col>
                          <p className="font8bitOperatorPlusSCBold" style={{ fontSize: '2em' }}>{teamWinner.nome}</p>
                        </Col>
                        <Col>
                          <p className="font8bitOperatorPlusSCBold" style={{ fontSize: '2em' }}>{teamWinner.message}</p>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col>
              <Row className="rowFlexBattle">
                <Button className="buttonYellow" size='large' onClick={this.props.store.balancingTimes ? this.toRankingBalancePage : this.state.isFinal ? this.toFinalPage : this.toRankingPage}>
                <span className="fontVcrOsdMono" style={{ fontSize: '2em' }}>{ this.props.store.balancingTimes ? this.props.labels.LABEL_BALANCE : this.props.labels.LABEL_RANKING}</span>
                </Button>
              </Row>
            </Col>
          </Row>
        </div>;
    }

    return elementJSX;
  }
}