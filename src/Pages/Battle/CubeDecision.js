import React, { Component, } from 'react';
import { Row, Col, Button } from 'antd';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";

import '../../../public/css/cube.css';


import vsBattleImage from '../../../public/images/pictures/vs-img.png';

import face1 from '../../../public/images/cube/cube-1.png';
import face2 from '../../../public/images/cube/cube-2.png';
import face3 from '../../../public/images/cube/cube-3.png';
import face4 from '../../../public/images/cube/cube-4.png';
import face5 from '../../../public/images/cube/cube-5.png';
import face6 from '../../../public/images/cube/cube-6.png';

/**
 * Componente Responsável por mostrar as opções iniciais
 * 
 */
@observer
@autobind
export default class CubeDecision extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = CubeDecision.initialState;
  }

  /**
 * Recupera o estado inicial do componente.
 */
  static get initialState() {
    return {
      initialise: false,
      cubeFaces: [
        "rotateY(-0deg) rotateX(0deg)",
        "rotateY(-270deg) rotateX(0deg)",
        "rotateY(-180deg) rotateX(0deg)",
        "rotateY(-0deg) rotateX(270deg)",
        "rotateY(-90deg) rotateX(0deg)",
        "rotateY(-0deg) rotateX(90deg)",
      ],
      cubeTeamPoint: 0,
      cubeTeamVsPoint: 0,
      showCube: false,
      diceRolling: false,
      isTurnTeam: true,
      isTurnTeamVS: false,
      diceRollingTimes: 0
    };
  }


  /**
   * FUnção respons´avel por resetar a animação do cubo a cada click
   */
  resetCubeAnimate() {
    let traveler = document.getElementById('traveler');
    let bouncer = document.getElementById('bouncer');
    traveler.style.webkitAnimation = 'none';
    bouncer.style.webkitAnimation = 'none';
    setTimeout(function () {
      traveler.style.webkitAnimation = '';
      bouncer.style.webkitAnimation = '';
    }, 10);
  }

  /**
   * Função responsável por setar a face e o número na state por meio de uma aleatorização depois de 6 segundos de animação.
   * 
   * @param {object} cubeIterationsInterval 
   */
  setRandomFaceNumber(cubeIterationsInterval) {
    setTimeout(() => {
      clearInterval(cubeIterationsInterval);
      let cubeChoose = Math.floor((Math.random() * 6));
      const box = document.querySelector("section");
      box.style.transform = this.state.cubeFaces[cubeChoose];
      setTimeout(() => {
        this.setState({
          cubeTeamPoint: this.state.cubeTeamPoint + (this.state.isTurnTeam ? ++cubeChoose : 0),
          cubeTeamVsPoint: this.state.cubeTeamVsPoint + (this.state.isTurnTeamVS ? ++cubeChoose : 0),
          cube: ++cubeChoose,
          isTurnTeamVS: !this.state.isTurnTeamVS,
          isTurnTeam: !this.state.isTurnTeam
        });

        setTimeout(() => {
          this.checkWinner(this.state.cubeTeamPoint == this.state.cubeTeamVsPoint);
        }, 3000);
      }, 3000);
    }, 6000);
  }

  /**
   * Função responsavel por realizar a animação do dado rolando no próprio eixo.
   */
  rollTheDice() {
    let xDir = "rotateY";
    let yDir = "rotateX";

    if (this.state.showCube) {
      this.resetCubeAnimate();
    }

    this.setState({
      showCube: true,
      diceRolling: true
    });

    var cubeIterationsInterval = setInterval(() => {
      const midX = Math.floor((Math.random() * 360)) - document.getElementById("cubeDecision").clientWidth / 2;
      const midY = Math.floor((Math.random() * 360)) - document.getElementById("cubeDecision").clientHeight / 2;

      const box = document.querySelector("section");
      box.style.transform = xDir + "(" + midX + "deg) " + yDir + "(" + midY + "deg)";
    }, 1000);

    document.querySelectorAll("select").forEach(select => {
      select.addEventListener("change", function () {
        if (this.name == "xDir") {
          xDir = this.value
        } else {
          yDir = this.value
        }
      });
    });

    this.setRandomFaceNumber(cubeIterationsInterval);
  }

  /**
 * Executado sempre que ocorre uma mudança no props ou ao montar o componente.
 * 
 * @param {object} props 
 * @param {object} state 
 */
  static getDerivedStateFromProps(props, state) {
    return CubeDecision.initValuesOnRender(props, state);
  }

  /**
   * Verifica se o vencedor já foi definido pelo jogar do dado.
   * 
   * @param {boolean} tied 
   */
  checkWinner(tied) {
    if (!tied && (this.state.diceRollingTimes % 2)) {
      //times em um estado observado pela store
      let battleTeam1ProxyMobx = this.props.store.teams[this.props.store.battleLevel][this.props.store.nextBattle.team.index];
      let battleTeam2ProxyMobx = this.props.store.teams[this.props.store.battleLevel][this.props.store.nextBattle.teamVs.index];

      if (this.state.cubeTeamPoint > this.state.cubeTeamVsPoint) {
        ++battleTeam1ProxyMobx.victoriesNumber;
        //times em um estado não observado pela store
        let battleTeam1 = this.props.store.teamsData[this.props.store.battleLevel][this.props.store.nextBattle.team.index];

        //verifica se não se trata de uma batalha de balanceamento
        if (!this.props.store.balancingTimes) {
          if (this.props.store.teams[this.props.store.battleLevel + 1]) {
            //deve ser adicionado um time que não esteja sendo observado pela store.
            this.props.store.teams[this.props.store.battleLevel + 1].push(battleTeam1);
          } else {
            this.props.store.teams[this.props.store.battleLevel + 1] = [battleTeam1];
          }
        }

        battleTeam1ProxyMobx.winner = true;
        battleTeam2ProxyMobx.winner = false;
      } else {
        ++battleTeam2ProxyMobx.victoriesNumber;
        //times em um estado não observado pela store
        let battleTeam2 = this.props.store.teamsData[this.props.store.battleLevel][this.props.store.nextBattle.teamVs.index];

        //verifica se não se trata de uma batalha de balanceamento
        if (!this.props.store.balancingTimes) {
          if (this.props.store.teams[this.props.store.battleLevel + 1]) {
            //deve ser adicionado um time que não esteja sendo observado pela store.
            this.props.store.teams[this.props.store.battleLevel + 1].push(battleTeam2);
          } else {
            this.props.store.teams[this.props.store.battleLevel + 1] = [battleTeam2];
          }
        }

        battleTeam2ProxyMobx.winner = true;
        battleTeam1ProxyMobx.winner = false;
      }

      //Atualiza o balanceamento caso seja uma batalha de balance
      if (this.props.store.balancingTimes) {
        this.props.centralCommand.executeAction('./GincanaController', 'updateTeamsBalaced');
      }

      this.toResultBattlePage();
    } else {
      this.setState({
        diceRolling: false,
        diceRollingTimes: ++this.state.diceRollingTimes
      });
    }
  }

  /**
   * Abre a tela de resultado da batalha
   */
  toResultBattlePage() {
    document.getElementById('cubeDecision').className = 'goOutPage';
    setTimeout(() => { this.props.store.route = this.props.constants.RESULT_BATTLE_KEY }, 1500);
  }

  /**
   * Inicializa as informações importantes para lançamento das questões
   * 
   */
  static initValuesOnRender(props, state) {
    if (props.renderElement && !state.initialise) {
      state.initialise = true;
    } else if (!props.renderElement) {
      state = CubeDecision.initialState;
    }

    return state;
  }

  /**
  * Renderiza o componente
  */
  render() {
    //variveis observadas da store

    let elementJSX = <div />;
    if (this.props.renderElement && this.state.initialise) {

      let battleTeam1 = this.props.store.teamsData[this.props.store.battleLevel][this.props.store.nextBattle.team.index];
      let battleTeam2 = this.props.store.teamsData[this.props.store.battleLevel][this.props.store.nextBattle.teamVs.index];


      elementJSX =
        <div className="goInPage" id="cubeDecision">
          <Row className="borderFrame">
            <Col>
              <Row className="rowFlexBattle">
                <Col>
                  <span className="fontKarmaFuture" style={{ fontSize: '3em', margin: '10px' }}>{this.props.labels.LABEL_GOOD_LUCK}</span>
                </Col>
              </Row>
              <Row className="rowFlexBattle">
                <Col className="rowFlex">
                  <span className="fontKarmaFuture" style={{ fontSize: '2em', margin: '10px' }}>{battleTeam1.nome} <span className="fontKarmaFuture" style={{ fontSize: '1.5em', color: "yellow" }}>{this.props.store.nextBattle.team.tempVictories}</span> X <span className="fontKarmaFuture" style={{ fontSize: '1.5em', color: "yellow" }}>{this.props.store.nextBattle.teamVs.tempVictories}</span> {battleTeam2.nome}</span>
                </Col>
              </Row>
            </Col>
            <Col className="cubeDecisionTeams">
              <Row className="rowFlexBattle">
                <Col style={{ width: "43.5%" }} className={this.state.isTurnTeam ? "animateTeamBattleCube" : ""}>
                  <Row>
                    <Col className="rowFlex">
                      <img className="sizeTeamBattleCube" src={"data:image/jpeg;" + battleTeam1.image} />
                    </Col>
                    <Col>
                      <Row className="teamBattleInfo">
                        <Col>
                          <p className="font8bitOperatorPlusSCBold">{battleTeam1.nome}</p>
                        </Col>
                        <Col>
                          <p className="font8bitOperatorPlusSCBold">{battleTeam1.message}</p>
                        </Col>
                        <Col>
                          <span className="fontKarmaFuture" style={{ fontSize: '3em', margin: '10px' }}>{this.props.labels.LABEL_PTS}-{this.state.cubeTeamPoint}</span>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
                <Col style={{ width: "15%" }}>
                  <img className="imgTeamBattleVS" src={vsBattleImage} />
                </Col>
                <Col style={{ width: "43.5%" }} className={this.state.isTurnTeamVS ? "animateTeamBattleCube" : ""}>
                  <Row>
                    <Col className="rowFlex">
                      <img className="sizeTeamBattleCube" src={"data:image/jpeg;" + battleTeam2.image} />
                    </Col>
                    <Col>
                      <Row className="teamBattleInfo">
                        <Col>
                          <p className="font8bitOperatorPlusSCBold">{battleTeam2.nome}</p>
                        </Col>
                        <Col>
                          <p className="font8bitOperatorPlusSCBold">{battleTeam2.message}</p>
                        </Col>
                        <Col>
                          <span className="fontKarmaFuture" style={{ fontSize: '3em', margin: '10px' }}>{this.props.labels.LABEL_PTS}-{this.state.cubeTeamVsPoint}</span>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col>
              {this.state.diceRolling ? <div /> :
                <Row className="rowFlex">
                  <Button className="buttonYellow" size='large' onClick={this.rollTheDice}><span className="fontVcrOsdMono" style={{ fontSize: '2em' }}>{this.props.labels.LABEL_DICE_ROLL}</span></Button>
                </Row>
              }
            </Col>
            {this.state.showCube ? <Col id="traveler">
              <div className="cubeArea" id="bouncer">
                <section>
                  <div className="face face-cover" style={{ backgroundImage: `url(${face1})` }}></div>
                  <div className="face face-spine" style={{ backgroundImage: `url(${face2})` }}></div>
                  <div className="face face-back" style={{ backgroundImage: `url(${face3})` }}></div>
                  <div className="face face-top" style={{ backgroundImage: `url(${face4})` }}></div>
                  <div className="face face-side" style={{ backgroundImage: `url(${face5})` }}></div>
                  <div className="face face-bottom" style={{ backgroundImage: `url(${face6})` }}></div>
                </section>
              </div>
            </Col> : <div />}
          </Row>
        </div>;
    }

    return elementJSX;
  }
}