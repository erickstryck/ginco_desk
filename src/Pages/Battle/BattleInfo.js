import React, { Component, } from 'react';
import { Row, Col, Button } from 'antd';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import '../../../public/css/battle/battle.css';


import trophyImage from '../../../public/images/pictures/trophy-img.png';
import startImage from '../../../public/images/pictures/start-img.png';

/**
 * Componente Responsável por mostrar as opções iniciais
 * 
 */
@observer
@autobind
export default class BattleInfo extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = BattleInfo.initialState;
  }

  /**
   * Executado sempre que ocorre uma mudança no props ou ao montar o componente.
   * 
   * @param {object} props 
   * @param {object} state 
   */
  static getDerivedStateFromProps(props, state) {
    return BattleInfo.initValuesOnRender(props, state);
  }

  /**
   * Recupera o estado inicial do componente.
   */
  static get initialState() {
    return {
      actualTeam: '',
      initialise: false,
      isTeamVS: true
    };
  }

  /**
   * Abre a tela de informacao da batalha
   */
  toBattleQuestionPage() {
    document.getElementById('battleInfo').className = 'goOutPage';
    setTimeout(() => { this.props.store.route = this.props.constants.BATTLE_QUEST_KEY }, 1500);
  }

  /**
   * Iniciar todas as variaveis no momento em que o conteudo do render deve ser mostrado.
   * 
   * @param {object} props 
   * @param {object} state 
   */
  static initValuesOnRender(props, state) {
    if (props.renderElement && !state.initialise) {
      state = BattleInfo.initialState;

      let actualTeam = props.store.teamsData[props.store.battleLevel][props.store.nextBattle.teamVs.index];

      if (props.store.nextBattle.step % 2) {
        state.isTeamVS = false;
        actualTeam = props.store.teamsData[props.store.battleLevel][props.store.nextBattle.team.index];
        ++props.store.nextBattle.team.questionIndex;
      } else {
        ++props.store.nextBattle.teamVs.questionIndex;
      }

      state.initialise = true;
      state.actualTeam = actualTeam;

    } else if (!props.renderElement) {
      state = BattleInfo.initialState;
    }

    return state;
  }

  /**
    * Renderiza o componente
    */
  render() {
    //variveis observadas da store

    let elementJSX = <div />;
    if (this.props.renderElement && this.state.initialise) {

      let imgVictories = "";
      let victories = this.state.isTeamVS ? this.props.store.nextBattle.teamVs.tempVictories : this.props.store.nextBattle.team.tempVictories;
      if (victories) {
        imgVictories = [];
        for (let x = 1; x <= victories; x++) {
          imgVictories.push(<Row className="slotItemArea" key={this.props.store.getId()}>
            <Col>
              <img className="animatedShake" src={trophyImage} />
            </Col>
          </Row>);
        }
      }

      elementJSX =
        <div className="goInPage" id="battleInfo">
          <Row className={"rowFlex " + this.props.class}>
            <Col className="battle rowColumn">
              <Row>
                <span className="fontKarmaFuture" style={{ fontSize: '4em', margin: '10px' }}>
                  {(this.props.store.nextBattle.step >= 5 ? this.props.labels.LABEL_LAST_QUESTION : (this.props.labels.LABEL_BATTLE_ROUND +
                    (this.state.isTeamVS ? this.props.store.nextBattle.teamVs.questionIndex : this.props.store.nextBattle.team.questionIndex)))}
                </span>
              </Row>
              <Row className="rowFlex">
                <Col className="battleInforImg">
                  <img className="imgTeamBattleInfo" src={"data:image/jpeg;" + this.state.actualTeam.image} />
                </Col>
                <Col>
                  <Row className="rowColumn">
                    <Col className="rowColumn">
                      <span className="font8bitOperatorPlusSCBold" style={{ fontSize: '1.5em', color: 'yellow' }}>{this.state.actualTeam.nome}</span>
                    </Col>
                    <Col className="rowColumn" style={{ padding: "1px" }}>
                      <span className="font8bitOperatorPlusSCBold">{this.state.actualTeam.message}</span>
                    </Col>
                    <Col className="rowColumn">
                      <br />
                      <span className="font8bitOperatorPlusSCBold">{imgVictories ? this.props.labels.LABEL_ACTUAL_BATTLE : this.props.labels.LABEL_WITHOUT_VICTORIES}</span>
                      <div className="rowFlex">
                        {imgVictories}
                      </div>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row className="rowFlex">
                <Button className="buttonFight" onClick={this.toBattleQuestionPage}>
                  <Row className="rowFlex">
                    <img src={startImage} />
                    <span className="fontVcrOsdMono" style={{ fontSize: '2em' }}>{this.props.labels.LABEL_START}</span>
                  </Row>
                </Button>
              </Row>
            </Col>
          </Row>
        </div>;
    }

    return elementJSX;
  }
}
