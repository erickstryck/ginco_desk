import React, { Component } from 'react';
import { Row, Col, Layout, Button, Modal } from 'antd';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import '../../../public/css/battle/battle.css';

import clockImg from '../../../public/images/pictures/clock-img.png';
import stopWatch from '../../../public/images/pictures/stop-watch-img.png';
import partyPoperImg from '../../../public/images/pictures/party-poper-img.png';
import awnserImageWrong from '../../../public/images/pictures/wrong-answer-img.png';

const { Header, Content } = Layout;

/**
 * Componente Responsável por mostrar as opções iniciais
 * 
 */
@observer
@autobind
export default class BattleQuestion extends Component {

	/**
	 * Construtor da classe
	 */
	constructor() {
		super();
		this.state = BattleQuestion.initialState;
	}


	/**
	* Recupera o estado inicial do componente.
	*/
	static get initialState() {
		return {
			time: '',
			interval: '',
			modalCheckAnswer: false,
			isAlternativeCorrect: '',
			rightAwnser: '',
			finish: false,
			question: "",
			actualTeam: "",
			teamVs: "",
			initialise: false,
			isTeamVS: true
		};
	}

	/**
 * Executado sempre que ocorre uma mudança no props ou ao montar o componente.
 * 
 * @param {object} props 
 * @param {object} state 
 */
	static getDerivedStateFromProps(props, state) {
		return BattleQuestion.initValuesOnRender(props, state);
	}

	/**
	 * Executa a função sempre que o componente está atualizando.
	 *
	 * @param {Object} prevProps
	 * @param {Object} prevState
	 */
	getSnapshotBeforeUpdate(prevProps, prevState) {
		if (prevProps.renderElement && prevState.time && prevState.time <= 0) {
			clearInterval(this.state.interval);
			prevState.modalCheckAnswer = false;
		} else if (prevProps.renderElement && !prevState.time) {
			prevState.time = prevProps.store.gincana.time;
		}
		return null;
	}

	/**
	 * Executa a função sempre que o componente é atualizado.
	 *
	 * @param {Object} prevProps
	 * @param {Object} prevState
	 */
	componentDidUpdate(prevProps, prevState) {

		if (prevProps.renderElement && prevState.time && prevState.time <= 0) {
			clearInterval(this.state.interval);
			if (!this.state.modalCheckAnswer && !this.state.finish) {
				this.handleModalCheckAnswer();
			}
		}

	}

	/**
	 * Responsável por trabalhar na contagem regressiva do relógio
	 */
	regressiveClock() {
		this.state.interval = setInterval(() => {
			this.setState({
				time: --this.state.time
			});
		}, 1000);
	}

	/**
	 * Verifica se a alternativa escolhida è a correta.
	 *
	 * @param {*} isCorrect
	 */
	verifyChoseQuestion(isCorrect) {
		clearInterval(this.state.interval);

		this.setState({
			modalCheckAnswer: true,
			isAlternativeCorrect: isCorrect
		});
	}

	/**
	 * Abre a tela de resultado da batalha
	 */
	toResultBattlePage() {
		document.getElementById('battleQuestion').className = 'goOutPage';
		setTimeout(() => { this.props.store.route = this.props.constants.RESULT_BATTLE_KEY }, 1500);
	}

	/**
	 * Verifica se o vencedor já foi definido.
	 * 
	 * @param {boolean} tied 
	 */
	checkWinner() {
		//times em um estado observado pela store
		let battleTeam1ProxyMobx = this.props.store.teams[this.props.store.battleLevel][this.props.store.nextBattle.team.index];
		let battleTeam2ProxyMobx = this.props.store.teams[this.props.store.battleLevel][this.props.store.nextBattle.teamVs.index];

		if (this.props.store.nextBattle.team.tempVictories > this.props.store.nextBattle.teamVs.tempVictories) {
			++battleTeam1ProxyMobx.victoriesNumber;

			//times em um estado não observado pela store
			let battleTeam1 = this.props.store.teamsData[this.props.store.battleLevel][this.props.store.nextBattle.team.index];

			//verifica se não se trata de uma batalha de balanceamento
			if (!this.props.store.balancingTimes) {
				if (this.props.store.teams[this.props.store.battleLevel + 1]) {
					//deve ser adicionado um time que não esteja sendo observado pela store.
					this.props.store.teams[this.props.store.battleLevel + 1].push(battleTeam1);
				} else {
					this.props.store.teams[this.props.store.battleLevel + 1] = [battleTeam1];
				}
			}

			battleTeam1ProxyMobx.winner = true;
			battleTeam2ProxyMobx.winner = false;
		} else {
			++battleTeam2ProxyMobx.victoriesNumber;

			//times em um estado não observado pela store
			let battleTeam2 = this.props.store.teamsData[this.props.store.battleLevel][this.props.store.nextBattle.teamVs.index];

			//verifica se não se trata de uma batalha de balanceamento
			if (!this.props.store.balancingTimes) {
				if (this.props.store.teams[this.props.store.battleLevel + 1]) {
					//deve ser adicionado um time que não esteja sendo observado pela store.
					this.props.store.teams[this.props.store.battleLevel + 1].push(battleTeam2);
				} else {
					this.props.store.teams[this.props.store.battleLevel + 1] = [battleTeam2];
				}
			}
			battleTeam2ProxyMobx.winner = true;
			battleTeam1ProxyMobx.winner = false;
		}

		//Atualiza o balanceamento caso seja uma batalha de balance
		if (this.props.store.balancingTimes) {
			this.props.centralCommand.executeAction('./GincanaController', 'updateTeamsBalaced');
		}

		this.toResultBattlePage();
	}

	/**
	 * Realiza as ações necessária para o prosseguimento da gincana após uma escolha ou time-out
	 */
	processAnswer() {
		this.handleModalCheckAnswer();

		if (this.state.isAlternativeCorrect) {
			if (this.state.isTeamVS) {
				++this.props.store.nextBattle.teamVs.tempVictories;
			} else {
				++this.props.store.nextBattle.team.tempVictories;
			}
		}

		if (this.props.store.nextBattle.step == 6) {
			this.props.store.nextBattle.step = 1;

			if (this.props.store.nextBattle.teamVs.tempVictories != this.props.store.nextBattle.team.tempVictories) {
				this.checkWinner();
			} else {
				this.toCubeDecisionPage();
			}
		} else {
			++this.props.store.nextBattle.step;
			this.state.finish = true;

			this.toBattleInfoPage();
		}
	}

	/**
	 * Abre a tela de informação da batalha
	 */
	toBattleInfoPage() {
		document.getElementById('battleQuestion').className = 'goOutPage';
		setTimeout(() => { this.props.store.route = this.props.constants.BATTLE_INFO_KEY }, 1500);
	}

	/**
	 * Abre a tela de desempate pelo cubo
	 */
	toCubeDecisionPage() {
		document.getElementById('battleQuestion').className = 'goOutPage';
		setTimeout(() => { this.props.store.route = this.props.constants.CUBE_DECISION_KEY }, 1500);
	}

	/**
	 * Recupera um indice válido aleatório para ser utilizado na pergunta a ser utilizada na batalha
	 * @param {Array} questions 
	 */
	static getRandomIndex(questions) {
		let index = Math.floor((Math.random() * questions.length));
		if (questions[index].isUsed) {
			index = BattleQuestion.getRandomIndex(questions);
		}

		return index;
	}

	/**
	 * Manipulador de visualização do modal de verificação de resposta.
	 */
	handleModalCheckAnswer() {
		this.setState({
			modalCheckAnswer: !this.state.modalCheckAnswer
		});
	}

	/**
	 * Inicializa as informações importantes para lançamento das questões
	 * 
	 * @param {object} props 
	 * @param {object} state 
	 */
	static initValuesOnRender(props, state) {
		if (props.renderElement && !state.initialise) {

			let actualTeam = props.store.teams[props.store.battleLevel][props.store.nextBattle.teamVs.index];
			actualTeam.tempVictories = props.store.nextBattle.teamVs.tempVictories;
			let teamVs = props.store.teams[props.store.battleLevel][props.store.nextBattle.team.index];
			teamVs.tempVictories = props.store.nextBattle.team.tempVictories;

			if (props.store.nextBattle.step % 2) {
				state.isTeamVS = false;
				actualTeam = props.store.teams[props.store.battleLevel][props.store.nextBattle.team.index];
				actualTeam.tempVictories = props.store.nextBattle.team.tempVictories;
				teamVs = props.store.teams[props.store.battleLevel][props.store.nextBattle.teamVs.index];
				teamVs.tempVictories = props.store.nextBattle.teamVs.tempVictories;
			}

			state.question = BattleQuestion.getQuestion(teamVs);
			state.time = props.store.gincana.time;
			state.actualTeam = actualTeam;
			state.teamVs = teamVs;
			state.initialise = true;
		} else if (!props.renderElement) {
			state = BattleQuestion.initialState;
		}
		return state;
	}

	/**
	 * Retorna a pergunta que será utilizada na rodada.
	 * 
	 * @param {object} actualTeam 
	 */
	static getQuestion(actualTeam) {
		let questionIndex = BattleQuestion.getRandomIndex(actualTeam.perguntas);
		let question = actualTeam.perguntas[questionIndex];
		actualTeam.perguntas[questionIndex].isUsed = true;

		return question;
	}

	/**
	 * Retorna o componenent modal relacionado a informação da resposta selecionada.
	 */
	getComponenteModalCheckAnswer() {
		//Iniciar as mensagens antes de abrir o modal de resposta
		let messageModal = '';
		let imgCheckInfo = '';
		if (this.state.modalCheckAnswer) {
			messageModal = this.props.labels.LABEL_TIME_IS_OVER;
			imgCheckInfo = <img src={stopWatch} />;

			if (this.state.isAlternativeCorrect) {
				messageModal = this.props.labels.LABEL_RIGHT_ANSWER;
				imgCheckInfo = <img src={partyPoperImg} />;
			} else if (this.state.isAlternativeCorrect === false) {
				messageModal = this.props.labels.LABEL_WRONG_ANSWER;
				imgCheckInfo = <img src={awnserImageWrong} />;
			}
		}

		return (
			<Modal
				visible={this.state.modalCheckAnswer}
				maskClosable={false}
				closable={false}
				mask={true}
				className="modalCheckAnswer modalBGColorInfo"
				footer={null}
				width="60vw"
			>
				<Row className="rowColumn">
					<Col>
						<span className="fontKarmaFuture header">{messageModal}</span>
					</Col>
					<Col>
						{imgCheckInfo}
					</Col>
					<Col>
						<Row className="rowColumn">
							<Col>
								<span className="font8bitOperatorPlusSCBold rightAnswer">{this.props.labels.LABEL_CHECK_ANSWER}</span>
							</Col>
							<Col>
								<span className="font8bitOperatorPlusSCBold rightAnswer">{this.state.rightAwnser}</span>
							</Col>
						</Row>
					</Col>
					<Col>
						<Button className="buttonOrange" size='large' onClick={this.processAnswer}><span className="fontVcrOsdMono" style={{ fontSize: '2em' }}>{this.props.labels.LABEL_NEXT_ROUND}</span></Button>
					</Col>
				</Row>
			</Modal>
		);
	}

	/**
		* Renderiza o componente
		*/
	render() {
		//variveis observadas da store

		let elementJSX = <div />;

		if (this.props.renderElement && this.state.initialise) {

			//inicia o relogio regressivo
			if (!this.state.interval && this.state.time) {
				this.regressiveClock();
			}

			var alternatives = [];
			this.state.question.alternativas.forEach((alternative, index) => {
				let labelAlternative = String.fromCharCode(97 + index).toUpperCase() + ") " + alternative.descricao;

				this.state.rightAwnser = alternative.isCorreta ? labelAlternative : this.state.rightAwnser;
				alternatives.push(
					<Row key={this.props.store.getId()} className="rowFlex" onClick={this.verifyChoseQuestion.bind(this, alternative.isCorreta, labelAlternative)}>
						<Col className="arrow hide"></Col>
						<Col>
							<p className="font8bitOperatorPlusSCBold">{labelAlternative}</p>
						</Col>
					</Row>
				);
			});

			elementJSX =
				<div className="goInPage" id="battleQuestion">
					<Row className={"rowFlex " + this.props.class}>
						{this.getComponenteModalCheckAnswer()}
						<Col className="battle rowColumn battleQuestion">
							<Col className="rowFlex">
								<span className="fontKarmaFuture" style={{ fontSize: '1.5em', margin: '10px', color: "yellow" }}>{this.state.actualTeam.nome} <span className="fontKarmaFuture" style={{ fontSize: '1.5em' }}>{this.state.actualTeam.tempVictories}</span> X <span className="fontKarmaFuture" style={{ fontSize: '1.5em' }}>{this.state.teamVs.tempVictories}</span> {this.state.teamVs.nome}</span>
							</Col>
							<Row className="rowFlex">
								<Col>
									<span className="fontKarmaFuture" style={{ fontSize: '2.5em', margin: '10px' }}>{this.state.actualTeam.nome}</span>
									<Row>
										<Col className="rowColumn">
											<span className="font8bitOperatorPlusSCBold" style={{ color: 'yellow' }}>
												{(this.props.store.nextBattle.step >= 5 ? this.props.labels.LABEL_LAST_QUESTION : (this.props.labels.LABEL_BATTLE_ROUND +
													(this.state.isTeamVS ? this.props.store.nextBattle.teamVs.questionIndex : this.props.store.nextBattle.team.questionIndex)))}
											</span>
										</Col>
									</Row>
								</Col>
								<Col className="clock">
									<Row className="rowFlex">
										<Col>
											<img src={clockImg} />
										</Col>
										<Col style={{ width: "8em" }} className={this.state.time < 10 ? "lastSeconds" : ""}>
											<span className="fontKarmaFuture" style={{ fontSize: '4em', margin: '10px' }}>{this.state.time < 10 ? "0" + this.state.time : this.state.time}</span>
										</Col>
									</Row>
								</Col>
							</Row>
							<Row className="battle bgWhite">
								<Col className="rowColumn">
									<span className="font8bitOperatorPlusSCBold">{this.props.labels.LABEL_QUESTION}</span>
								</Col>
							</Row>
							<Row className="rowFlex">
								<Col className="rowColumn question">
									<p className="font8bitOperatorPlusSCBold">{this.state.question.enunciado}</p>
								</Col>
							</Row>
							<Row className="battle bgWhite" style={{ marginTop: 0 }}>
								<Col className="rowColumn">
									<span className="font8bitOperatorPlusSCBold">{this.props.labels.LABEL_ALTERNATIVES}</span>
								</Col>
							</Row>
							<Row className="rowFlex">
								<Col className="rowColumn relevantInfoBlock">
									{alternatives}
								</Col>
							</Row>
						</Col>
					</Row>
				</div>;
		}

		return elementJSX;
	}
}
